package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.Book;
import org.springframework.stereotype.Repository;


@Repository
public class BookDao extends AbstractDao<Book> {
    public Book addBook(Book book) {
        em.persist(book);
        em.flush();
        return book;
    }
}
