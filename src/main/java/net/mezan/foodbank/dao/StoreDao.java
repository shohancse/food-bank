package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.Store;
import net.mezan.foodbank.enumerator.Status;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class StoreDao extends AbstractDao<Store> {

    public List<Store> getPopularStores() {

        String sql = "SELECT * FROM store WHERE id IN(SELECT DISTINCT store_id FROM product WHERE avg_rating >= 3.5)";

        List<Store> popularStores = em.createNativeQuery(sql, Store.class).getResultList();

        return popularStores;
    }

    public Store doesExist(String searchKeyword){
        List<Store> list = em.createQuery("FROM " + Store.class.getName() + " WHERE " + "name" + " " +
                "LIKE :searchKeyword", Store.class)
                .setParameter("searchKeyword", "%"+searchKeyword+"%")
                .getResultList();

        return !list.isEmpty() ? list.get(0) : null;
    }

    public List<Store> findAllStoreOrderByStatus() {

        List<Store> storeList = findAllBy("status",Status.PENDING);
        storeList.addAll(findAllBy("status",Status.REJECTED));
        storeList.addAll(findAllBy("status",Status.APPROVED));
        return storeList;
    }
}