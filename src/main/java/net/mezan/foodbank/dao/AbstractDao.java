package net.mezan.foodbank.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@SuppressWarnings("unchecked")
public class AbstractDao<T extends Serializable> {

    private Class<T> clazz;

    @PersistenceContext
    public EntityManager em;

    public AbstractDao() {
        clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    public void flushEntityManager() {
        em.flush();
    }

    public Long findCount() {
        Query query = em.createQuery("SELECT COUNT(*)" + "FROM " + clazz.getName());
        return (Long) query.getSingleResult();
    }

    public T find(long id) {
        return em.find(clazz, id);
    }

    public T findBy(String fieldName, Object value) {
        List<T> list = (List<T>) em.createQuery("FROM " + clazz.getName() + " WHERE " + fieldName + " = :val")
                .setParameter("val", value)
                .getResultList();

        return !list.isEmpty() ? list.get(0) : null;
    }

    public T findBy(String fieldName1, Object value1, String fieldName2, Object value2) {
        List<T> list = (List<T>) em.createQuery("FROM " + clazz.getName() + " WHERE " + fieldName1 + " = :val1 and " + fieldName2 + " = :val2")
                .setParameter("val1", value1)
                .setParameter("val2", value2)
                .getResultList();

        return !list.isEmpty() ? list.get(0) : null;
    }

    public List<T> findAll() {
        return em.createQuery("FROM " + clazz.getName())
                .getResultList();
    }


    public List<T> findAll(int page, int size) {
        return em.createQuery("FROM " + clazz.getName())
                .setFirstResult(page * size)
                .setMaxResults(size)
                .getResultList();
    }

    public Long countRows(String fieldName, Object value) {
        Query query = em.createQuery("SELECT COUNT(id)" + "FROM " + clazz.getName() + " WHERE " + fieldName + " = :val")
                .setParameter("val", value);
        return (Long) query.getSingleResult();
    }

    public List<T> findAllBy(String fieldName, Object value) {
        return (List<T>) em.createQuery("FROM " + clazz.getName() + " WHERE " + fieldName + " = :val")
                .setParameter("val", value)
                .getResultList();
    }

    public List<T> findAllBy(String fieldName, Object value, int page, int size) {
        return (List<T>) em.createQuery("FROM " + clazz.getName() + " WHERE " + fieldName + " = :val")
                .setParameter("val", value)
                .setFirstResult(page * size)
                .setMaxResults(size)
                .getResultList();
    }

    public List<T> findAllBy(Map<String, Object> fieldMap) {
        String queryString = "FROM " + clazz.getName();
        List<Object> values = new ArrayList<>();
        int fieldIndex = 0;
        for (Map.Entry<String, Object> keyValuePairs : fieldMap.entrySet()) {
            String fieldName = keyValuePairs.getKey();
            Object value = keyValuePairs.getValue();
            if (fieldIndex != 0) {
                queryString += " AND ";
            } else {
                queryString += " WHERE ";
            }
            queryString += fieldName + " = :?";
            values.add(value);
            fieldIndex += 1;
        }

        Query query = em.createNativeQuery(queryString);
        fieldIndex = 0;
        for (Object value : values) {
            query.setParameter(++fieldIndex, value);
        }

        return query.getResultList();
    }

    public void save(T entity) {
        em.persist(entity);
    }

    public void update(T entity) {
        em.merge(entity);
    }

    public void delete(T entity) {
        em.remove(entity);
    }

    public void deleteDetached(T entity) {
        em.remove(em.contains(entity) ? entity : em.merge(entity));
    }

    public void delete(long entityId) {
        T entity = find(entityId);
        delete(entity);
    }

    public void deleteBy(String fieldName, Object value) {

        em.createQuery("DELETE FROM " + clazz.getName() + " WHERE " + fieldName + " = :val")
                .setParameter("val", value)
                .executeUpdate();

    }

    public List<T> findObjectsGreaterAndAbove(String fieldName, Object value) {
        return (List<T>) em.createQuery("FROM " + clazz.getName() + " WHERE " + fieldName + " >= :val")
                .setParameter("val", value)
                .getResultList();
    }

    public List<T> findAllBySorted(String fieldName, Object value) {
        return (List<T>) em.createQuery("FROM " + clazz.getName() + " WHERE " + fieldName + " = :val ORDER BY(id) DESC")
                .setParameter("val", value)
                .getResultList();
    }
}