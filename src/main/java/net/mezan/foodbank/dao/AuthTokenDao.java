package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.AuthToken;
import org.springframework.stereotype.Repository;


@Repository
public class AuthTokenDao extends AbstractDao<AuthToken> {
}
