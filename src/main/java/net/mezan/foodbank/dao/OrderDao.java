package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.Order;
import net.mezan.foodbank.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class OrderDao extends AbstractDao<Order> {

    @SuppressWarnings("unchecked")
    public Order findMostRecentOrder(User user) {
        List<Order> list = (List<Order>) em.createQuery("FROM Order o WHERE user = :user  order by o.checkoutTime desc")
                .setParameter("user", user)
                .getResultList();

        return !list.isEmpty() ? list.get(0) : null;
    }

}
