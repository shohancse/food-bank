package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.SaleLog;
import org.springframework.stereotype.Repository;


@Repository
public class SaleLogDao extends AbstractDao<SaleLog> {
}
