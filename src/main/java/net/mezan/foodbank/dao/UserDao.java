package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.User;
import org.springframework.stereotype.Repository;


@Repository
public class UserDao extends AbstractDao<User> {
}
