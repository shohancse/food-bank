package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.SubCategory;
import org.springframework.stereotype.Repository;


@Repository
public class SubCategoryDao extends AbstractDao<SubCategory> {

}
