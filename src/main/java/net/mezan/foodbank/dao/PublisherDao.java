package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.Publisher;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class PublisherDao extends AbstractDao<Publisher> {

    public List<Publisher> getPopularPublishers() {

        String sql = "SELECT * FROM publisher WHERE id IN(SELECT publisher_id FROM book WHERE id IN(SELECT book_id FROM " +
                "product JOIN book_author USING(book_id) WHERE avg_rating >= 3.5) GROUP BY publisher.name )";

        List<Publisher> popularPublishers = em.createNativeQuery(sql, Publisher.class).getResultList();

        return popularPublishers;
    }

    public Publisher doesExist(String searchKeyword) {
        List<Publisher> list = (List<Publisher>) em.createQuery("FROM " + Publisher.class.getName() + " WHERE " + "name" + " " +
                "LIKE :searchKeyword")
                .setParameter("searchKeyword", "%" + searchKeyword + "%")
                .getResultList();

        return !list.isEmpty() ? list.get(0) : null;
    }

    public Publisher addPublisher(Publisher publisher) {
        em.persist(publisher);
        em.flush();
        return publisher;
    }
}