package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.UserProductRating;
import org.springframework.stereotype.Repository;


@Repository
public class UserProductRatingDao extends AbstractDao<UserProductRating> {
}
