package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.VerificationToken;
import org.springframework.stereotype.Repository;


@Repository
public class VerificationTokenDao extends AbstractDao<VerificationToken> {
}
