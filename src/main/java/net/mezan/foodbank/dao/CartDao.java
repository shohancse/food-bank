package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.Cart;
import org.springframework.stereotype.Repository;


@Repository
public class CartDao extends AbstractDao<Cart> {

}
