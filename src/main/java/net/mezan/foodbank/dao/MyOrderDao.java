package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.OrderedProduct;
import net.mezan.foodbank.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class MyOrderDao extends AbstractDao<OrderedProduct>{

    public List<OrderedProduct> getMyOrders(User user, int page, int size) {
        String sql = "SELECT * FROM ordered_product WHERE product_id IN(SELECT id FROM product WHERE store_id IN(SELECT id" +
                " FROM store WHERE user_id = "+user.getId()+"))";

        List<OrderedProduct> orderedProducts = em.createNativeQuery(sql, OrderedProduct.class)
                .setFirstResult(page * size)
                .setMaxResults(size)
                .getResultList();

        return orderedProducts;

    }
}
