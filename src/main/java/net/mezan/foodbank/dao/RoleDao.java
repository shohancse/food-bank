package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.Role;
import org.springframework.stereotype.Repository;


@Repository
public class RoleDao extends AbstractDao<Role> {
}
