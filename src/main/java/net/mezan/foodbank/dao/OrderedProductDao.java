package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.OrderedProduct;
import net.mezan.foodbank.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class OrderedProductDao extends AbstractDao<OrderedProduct> {

    @SuppressWarnings("unchecked")
    public List<OrderedProduct> findAllUserPurchasedProduct(User user) {
        return (List<OrderedProduct>) em.createQuery("SELECT DISTINCT p FROM Order o LEFT JOIN o.orderedProductList p WHERE o.user = :user")
                .setParameter("user", user)
                .getResultList();
    }
}
