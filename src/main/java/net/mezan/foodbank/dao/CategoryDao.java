package net.mezan.foodbank.dao;

import net.mezan.foodbank.domain.Category;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CategoryDao extends AbstractDao<Category> {

    public List<Category> getPopularCategories() {

        String sql = "SELECT * FROM category WHERE id IN(SELECT category_id FROM subcategory WHERE id IN " +
                "(SELECT sub_category_id FROM product JOIN book_sub_category USING(book_id) WHERE avg_rating >= 3.5) " +
                "GROUP BY category.name)";

        List<Category> popularCategories = em.createNativeQuery(sql, Category.class).getResultList();

        return popularCategories;
    }

    public Category doesExist(String searchKeyword){
        List<Category> list = em.createQuery("FROM " + Category.class.getName() + " WHERE " + "name" + " " +
                "LIKE :searchKeyword", Category.class)
                .setParameter("searchKeyword", "%"+searchKeyword+"%")
                .getResultList();

        return !list.isEmpty() ? list.get(0) : null;
    }
}