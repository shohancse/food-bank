package net.mezan.foodbank.web.controller;

import net.mezan.foodbank.service.admin.AdminCategoryService;
import net.mezan.foodbank.domain.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@Controller
public class CategoryViewController {

    @Autowired
    private AdminCategoryService categoryService;

    @GetMapping(value = "/categories")
    public String getCategories(Model model, HttpServletRequest request) {

        int pageNumber = 1;
        int pageSize = 6;

        if (request.getParameter("page") != null) {
            pageNumber = Integer.parseInt(request.getParameter("page"));
        }

        long recordsCount = categoryService.findCount();
        long noOfPages = (int) Math.ceil(recordsCount * 1.0 / pageSize);
        List<Category> categoryList = categoryService.getCategories(pageNumber - 1, pageSize);

        model.addAttribute("noOfPages", noOfPages);
        model.addAttribute("currentPage", pageNumber);
        model.addAttribute("viewList", categoryList);
        model.addAttribute("viewIcon", "categoryicon.jpg");
        model.addAttribute("viewType", "Category");
        model.addAttribute("viewUrl", "categories");

        return "navigation_view";
    }

    @GetMapping(value="/categories/search")
    public String categorySearch(Model model, @RequestParam("searchKeyWord")String searchKeyWord, HttpServletRequest request) {

        String pageToLoad = "navigation_view";
        if (searchKeyWord.equals("")) {
            pageToLoad = "redirect:/categories";
        }
        else {

            List<Category> categoryList = new ArrayList<>();
            categoryList.add(categoryService.getSearchResult(formatSearchKeyWord(searchKeyWord)));

            model.addAttribute("noOfPages", 1);
            model.addAttribute("currentPage", 1);
            model.addAttribute("viewList", categoryList);
            model.addAttribute("viewIcon", "categoryicon.jpg");
            model.addAttribute("viewType", "Category");
            model.addAttribute("viewUrl", "categories");
        }

        return pageToLoad;
    }
    private String formatSearchKeyWord(String searchKeyWord) {
        return searchKeyWord.substring(0, 1).toUpperCase() + searchKeyWord.substring(1).toLowerCase();
    }

    @GetMapping(value = "/categories/{id}")
    public String categoryProduct(Model model, @PathVariable(value = "id") long categoryId) {
        return "redirect:/product/category/" + categoryId;
    }
}
