package net.mezan.foodbank.web.controller.storekeeper;

import net.mezan.foodbank.service.ActivityLogService;
import net.mezan.foodbank.web.helper.AuthHelper;
import net.mezan.foodbank.domain.User;
import net.mezan.foodbank.service.storekeeper.MyUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;


@Controller
public class MyActivityLogController {

    @Autowired
    ActivityLogService activityLogService;

    @Autowired
    MyUserService myUserService;

    @GetMapping(value = "/mystores/activity/{page}")
    public String getActivityLog(Locale locale,
                                 HttpServletRequest request,
                                 @PathVariable(value = "page") int page,
                                 Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        int resultSize = 10;

        User user = myUserService.getUser(userId);
        model.addAttribute("activityLog", activityLogService.getMyActivityLog(user, page, resultSize));
        model.addAttribute("countNextLogs", activityLogService.countMyNextLogs(user, page + 1, resultSize));
        model.addAttribute("page", page);

        return "store/activityLog";
    }
}
