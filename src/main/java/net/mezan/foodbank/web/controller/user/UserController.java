package net.mezan.foodbank.web.controller.user;

import net.mezan.foodbank.service.ActivityLogService;
import net.mezan.foodbank.service.UserOrderService;
import net.mezan.foodbank.util.URL;
import net.mezan.foodbank.util.View;
import net.mezan.foodbank.web.command.UserProfileForm;
import net.mezan.foodbank.web.helper.AuthHelper;
import net.mezan.foodbank.enumerator.ActivityType;
import net.mezan.foodbank.service.UserService;
import net.mezan.foodbank.web.helper.UserHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;


@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserOrderService userOrderService;

    @Autowired
    private ActivityLogService logService;

    @GetMapping(value = URL.USER_OVERVIEW)
    public String showUserHome(Locale locale,
                               HttpServletRequest request,
                               Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);

        model.addAttribute("cartCount", 2);
        model.addAttribute("reviewCount", 2);
        model.addAttribute("purchaseCount", 2);
        model.addAttribute("recentOrder", userOrderService.getMostRecentOrderInfo(userId));
        UserHelper.bindUserCard(userService, request, model);
        return View.USER_HOME;
    }

    @GetMapping(value = URL.USER_PROFILE)
    public String showUserProfile(Locale locale,
                                  HttpServletRequest request,
                                  Model model) {

        UserHelper.bindUserProfile(userService, request, model);
        UserHelper.bindUserCard(userService, request, model);
        return View.USER_PROFILE;
    }

    @GetMapping(value = URL.USER_PROFILE_EDIT)
    public String showUserProfileEdit(Locale locale,
                                      HttpServletRequest request,
                                      Model model) {

        UserHelper.bindUserProfile(userService, request, model);
        UserHelper.bindUserCard(userService, request, model);
        return View.USER_PROFILE_EDIT;
    }

    @PostMapping(value = URL.USER_PROFILE_EDIT)
    public String doUserProfileEdit(Locale locale,
                                    @Valid UserProfileForm userProfileForm,
                                    BindingResult result,
                                    HttpServletRequest request,
                                    Model model) {

        if (!result.hasErrors()) {
            long userId = AuthHelper.getUserIdFromSession(request);
            userService.updateUser(userId, userProfileForm);

            model.addAttribute("profileEditSuccess", true);

            UserHelper.bindUserProfile(userService, request, model);
            UserHelper.bindUserCard(userService, request, model);

            logService.insertActivityLog(userId, ActivityType.PROFILE_UPDATE, " Updated Profile.");
            return View.USER_PROFILE;
        }

        model.addAttribute("userProfileForm", userProfileForm);
        UserHelper.bindUserCard(userService, request, model);
        return View.USER_PROFILE_EDIT;
    }

    @GetMapping(value = URL.USER_ACTIVITY_LOG)
    public String showUserActivityLog(Locale locale,
                                      HttpServletRequest request,
                                      Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        model.addAttribute("logList", logService.getUserActivityLog(userId, 0, 10));
        UserHelper.bindUserCard(userService, request, model);
        return View.USER_ACTIVITY_LOG;
    }
}
