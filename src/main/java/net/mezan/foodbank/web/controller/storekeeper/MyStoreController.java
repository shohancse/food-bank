package net.mezan.foodbank.web.controller.storekeeper;

import net.mezan.foodbank.domain.Store;
import net.mezan.foodbank.domain.User;
import net.mezan.foodbank.service.ActivityLogService;
import net.mezan.foodbank.service.storekeeper.MyStoreService;
import net.mezan.foodbank.service.storekeeper.MyUserService;
import net.mezan.foodbank.enumerator.ActivityType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Date;
import java.util.Locale;


@Controller
public class MyStoreController {

    @Autowired
    MyStoreService myStoreService;

    @Autowired
    MyUserService myUserService;

    @Autowired
    ActivityLogService activityLogService;

    @GetMapping(value = "/mystores")
    public String getMyStores(Locale locale,
                              Model model,
                              HttpSession session) {
        model.addAttribute("storeList", myStoreService.getMyStores(Long.parseLong(session.getAttribute("USER_ID")
                .toString())));
        return "store/myStores";
    }

    @GetMapping(value = "/mystores/add")
    public String addStore(Locale locale,
                           HttpSession session) {
        return "store/addStore";
    }

    @PostMapping(value = "/mystores/add")
    public String addStore(Locale locale,
                           HttpSession session,
                           Model model,
                           @Valid @ModelAttribute Store store,
                           BindingResult result) {
        try {
            store.setUser(myUserService.getUser(Integer.parseInt(session.getAttribute("USER_ID").toString())));
            myStoreService.addStore(store);
            activityLogService.writeLog(ActivityType.INSERT, "You have Added a new Store : " + store.getName(),
                    new Date(), Long.parseLong(session.getAttribute("USER_ID").toString()));
            model.addAttribute("storeList", myStoreService.getMyStores(Long.parseLong(session.getAttribute("USER_ID")
                    .toString())));
            session.setAttribute("successMsg", "Store Added");

        } catch (Exception e) {
            session.setAttribute("errorMsg", "Store Name Already Exists");
            return "redirect:/mystores/add";
        }
        return "redirect:/mystores";
    }

    @GetMapping(value = "/mystores/{store_id}/update")
    public String updateStore(Locale locale,
                              Model model,
                              @PathVariable(value = "store_id") long store_id,
                              HttpSession session) {
        try {
            if (Long.parseLong(session.getAttribute("USER_ID").toString()) != myStoreService.getUserID(store_id)) {
                return "redirect:/mystores";
            }

            model.addAttribute("store", myStoreService.getStore(store_id));
        } catch (Exception e) {
            session.setAttribute("errorMsg", "Something went wrong, try again");
            return "redirect:/mystores";
        }
        return "store/updateStore";
    }

    @PostMapping(value = "/mystores/{store_id}/update")
    public String updateStore(Locale locale,
                              HttpSession session,
                              Model model,
                              @Valid @ModelAttribute Store store,
                              @PathVariable(value = "store_id") long store_id,
                              BindingResult result) {
        try {
            if (Long.parseLong(session.getAttribute("USER_ID").toString()) != myStoreService.getUserID(store_id)) {
                return "redirect:/mystores";
            }

            User user = myUserService.getUser(Long.parseLong(session.getAttribute("USER_ID").toString()));
            store.setUser(user);
            myStoreService.updateStore(store);
            activityLogService.writeLog(ActivityType.UPDATE, "You have Updated the information of the Store : "
                            + store.getName(), new Date()
                    , Long.parseLong(session.getAttribute("USER_ID").toString()));
            model.addAttribute("storeList", myStoreService.getMyStores(Long.parseLong(session.getAttribute("USER_ID")
                    .toString())));
            session.setAttribute("successMsg", "Store Updated");
        } catch (Exception e) {
            session.setAttribute("errorMsg", "Store with same name already exists");
            model.addAttribute("store", myStoreService.getStore(store_id));
            return "store/updateStore";
        }
        return "redirect:/mystores";
    }

    @PostMapping(value = "/mystores/{store_id}/delete")
    public String deleteStore(Locale locale,
                              HttpSession session,
                              Model model,
                              @PathVariable(value = "store_id") long store_id) {
        try {
            if (Long.parseLong(session.getAttribute("USER_ID").toString()) != myStoreService.getUserID(store_id)) {
                return "redirect:/mystores";
            }
            Store store = myStoreService.getStore(store_id);
            myStoreService.deleteStore(store);
            activityLogService.writeLog(ActivityType.DELETE, "You have Deleted the Store : " + store.getName(),
                    new Date(), Long.parseLong(session.getAttribute("USER_ID").toString()));
            session.setAttribute("successMsg", "Store Deleted");
            model.addAttribute("storeList", myStoreService.getMyStores(Long.parseLong(session.getAttribute("USER_ID")
                    .toString())));
            return "redirect:/mystores";
        } catch (Exception e) {
            session.setAttribute("errorMsg", "You can't delete the store, there is some products in it. " +
                    "Firstly delete the products and then try again");
            return "redirect:/mystores";
        }
    }
}