package net.mezan.foodbank.web.controller.cart;

import net.mezan.foodbank.dao.CartDao;
import net.mezan.foodbank.domain.User;
import net.mezan.foodbank.service.cart.CartService;
import net.mezan.foodbank.util.URL;
import net.mezan.foodbank.util.View;
import net.mezan.foodbank.web.command.DestinationForm;
import net.mezan.foodbank.web.helper.AuthHelper;
import net.mezan.foodbank.dao.UserDao;
import net.mezan.foodbank.domain.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;


@Controller
public class CartController {

    @Autowired
    CartDao cartDao;
    @Autowired
    CartService cartService;
    @Autowired
    UserDao userDao;

    @PostMapping(value = URL.CART_ADD)
    public String addToCart(HttpServletRequest request) {
        long productId = Long.parseLong(request.getParameter("productId"));
        long userId = AuthHelper.getUserIdFromSession(request);
        if (userId == 0) {
            return "redirect:" + URL.USER_LOGIN;
        }
        cartService.addToCart(userId, productId);

        return "redirect:" + URL.CART_VIEW;
    }

    @GetMapping(value = URL.CART_VIEW)
    public String showCart(Model model, HttpServletRequest request) {
        long userId = AuthHelper.getUserIdFromSession(request);

        List<Cart> cartList = cartDao.findAllBy("user_id", userId);
        if (cartList.size() > 0) {
            model.addAttribute("cartList", cartList);

            return View.CART;
        }

        return View.CART_EMPTY;
    }

    @PostMapping(value = URL.CART_REMOVE)
    public String removeCart(HttpServletRequest request) {
        long cartId = Long.parseLong(request.getParameter("cartId"));
        cartService.deleteCart(cartId);

        return "redirect:" + URL.CART_VIEW;
    }

    @PostMapping(value = URL.CART_EDIT)
    public String editCart(HttpServletRequest request) {
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        long cartId = Long.parseLong(request.getParameter("cartId"));

        cartService.updateCart(cartId, quantity);

        return "redirect:" + URL.CART_VIEW;
    }

    @GetMapping(value = URL.CART_SHIPPING)
    public String showShippingForm(Model model, HttpServletRequest request) {
        long userId = AuthHelper.getUserIdFromSession(request);

        List<Cart> cartList = cartDao.findAllBy("user_id", userId);
        if (cartList.size() > 0) {
            model.addAttribute("cartList", cartList);
            DestinationForm destination = new DestinationForm();
            model.addAttribute("destination", destination);

            return View.CART_SHIPPING;
        }

        return View.CART_EMPTY;
    }

    @PostMapping(value = URL.CART_SHIPPING)
    public String saveShippingAddress(HttpServletRequest request,
                                      @Valid DestinationForm destination,
                                      BindingResult result,
                                      Model model) {
        if (result.hasErrors()) {
            long userId = AuthHelper.getUserIdFromSession(request);
            for (ObjectError error : result.getAllErrors()) {
                System.out.println(error.getDefaultMessage());
            }
            List<Cart> cartList = cartDao.findAllBy("user_id", userId);
            if (cartList.size() > 0) {
                model.addAttribute("cartList", cartList);
                model.addAttribute("destination", destination);

                return View.CART_SHIPPING;
            }

            return View.CART_EMPTY;
        }
        String shippingAddress = destination.toString();
        request.getSession().setAttribute("shippingAddress", shippingAddress);

        return "redirect:" + URL.CART_SHIPPING_PAYMENT;
    }

    @GetMapping(value = URL.CART_SHIPPING_PAYMENT)
    public String showPaymentForm(Model model, HttpServletRequest request) {
        String destination = (String) request.getSession().getAttribute("shippingAddress");
        if (destination == null) {
            return "redirect:" + URL.CART_SHIPPING;
        }

        long userId = AuthHelper.getUserIdFromSession(request);

        User user = userDao.find(userId);
        List<Cart> cartList = cartDao.findAllBy("user_id", userId);
        if (cartList.size() > 0) {
            model.addAttribute("cartList", cartList);
            model.addAttribute("user", user);
            model.addAttribute("shippingCost", CartService.SHIPPING_COST);

            return View.CART_PAYMENT;
        }

        return View.CART_EMPTY;
    }

    @PostMapping(value = URL.CART_ORDER_SAVE)
    public String saveOrder(HttpServletRequest request) {
        long userId = AuthHelper.getUserIdFromSession(request);

        List<Cart> cartList = cartDao.findAllBy("user_id", userId);
        if (cartList.size() == 0) {
            return View.CART_EMPTY;
        }

        String bkashTransactionId = request.getParameter("transactionId");
        String paymentOption = request.getParameter("paymentOption");
        String destination = (String) request.getSession().getAttribute("shippingAddress");

        if (destination == null) {
            return "redirect:" + URL.CART_SHIPPING;
        }

        if (paymentOption == null || paymentOption.trim().equals("")) {
            return "redirect:" + URL.CART_SHIPPING_PAYMENT;
        }

        if ((paymentOption.equals("bkash")
                && (bkashTransactionId == null || bkashTransactionId.trim().equals("")))) {
            return "redirect:" + URL.CART_SHIPPING_PAYMENT;
        }

        long orderId = cartService.saveOrder(paymentOption, bkashTransactionId, destination, userId);
        cartService.deleteCartBy(userId);

        request.getSession().removeAttribute("shippingAddress");

        return "redirect:" + "/user/order-invoice/" + orderId;
    }
}
