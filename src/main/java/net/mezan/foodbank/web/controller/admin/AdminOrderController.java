package net.mezan.foodbank.web.controller.admin;

import net.mezan.foodbank.enumerator.OrderStatus;
import net.mezan.foodbank.service.ActivityLogService;
import net.mezan.foodbank.service.admin.AdminOrderService;
import net.mezan.foodbank.web.helper.AuthHelper;
import net.mezan.foodbank.domain.Order;
import net.mezan.foodbank.enumerator.ActivityType;
import net.mezan.foodbank.util.URL;
import net.mezan.foodbank.util.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


@Controller
public class AdminOrderController {

    @Autowired
    AdminOrderService adminOrderService;

    @Autowired
    ActivityLogService activityLogService;

    @GetMapping(value = URL.ADMIN_ORDERS)
    public String getOrders(Locale locale,
                            Model model) {

        model.addAttribute("pendingOrders", adminOrderService.getPendingOrders());
        model.addAttribute("processingOrders", adminOrderService.getProcessingOrders());
        return View.ADMIN_ORDERS;
    }

    @GetMapping(value = URL.ADMIN_ORDER_DETAILS)
    public String getOrder(@ModelAttribute Order order,
                           @PathVariable(value = "id") int id,
                           Locale locale,
                           Model model) {

        model.addAttribute("orderStatus", OrderStatus.values());
        model.addAttribute("order", adminOrderService.getOrder(order));
        return View.ADMIN_ORDER_DETAILS;
    }


    @PostMapping(value = URL.ADMIN_PROCESS_ORDER)
    public String processOrder(Locale locale,
                               HttpServletRequest request,
                               @PathVariable(value = "id") int id,
                               @ModelAttribute Order order,
                               Model model) {

        adminOrderService.processOrder(order);
        activityLogService.writeLog(ActivityType.UPDATE, "You have processed an order", new Date()
                , AuthHelper.getUserIdFromSession(request));

        return "redirect:" + URL.ADMIN_ORDER_DETAILS.replace("{id}", id + "");
    }

    @PostMapping(value = URL.ADMIN_DELIVER_ORDER)
    public String deliverOrder(Locale locale,
                               HttpServletRequest request,
                               @PathVariable(value = "id") int id,
                               @RequestParam(value = "date") String date,
                               @ModelAttribute Order order,
                               Model model) {
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date deliveryDate = format.parse(date);
            order.setDeliveryDate(deliveryDate);
            adminOrderService.deliverOrder(order, deliveryDate);
            activityLogService.writeLog(ActivityType.UPDATE, "You have delivered an order", new Date()
                    , AuthHelper.getUserIdFromSession(request));
        } catch (ParseException e) {
        }

        return "redirect:" + URL.ADMIN_ORDER_DETAILS.replace("{id}", id + "");
    }

    @PostMapping(value = URL.ADMIN_DISCARD_ORDER)
    public String discardOrder(Locale locale,
                               HttpServletRequest request,
                               @PathVariable(value = "id") int id,
                               @ModelAttribute Order order,
                               Model model) {

        adminOrderService.discardOrder(order);
        activityLogService.writeLog(ActivityType.UPDATE, "You have discarded an order", new Date()
                , AuthHelper.getUserIdFromSession(request));

        return "redirect:" + URL.ADMIN_ORDER_DETAILS.replace("{id}", id + "");
    }
}