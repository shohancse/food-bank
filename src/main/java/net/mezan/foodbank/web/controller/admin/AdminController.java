package net.mezan.foodbank.web.controller.admin;

import net.mezan.foodbank.domain.User;
import net.mezan.foodbank.service.ActivityLogService;
import net.mezan.foodbank.service.AuthorizationService;
import net.mezan.foodbank.web.command.LoginForm;
import net.mezan.foodbank.web.helper.AuthHelper;
import net.mezan.foodbank.enumerator.ActivityType;
import net.mezan.foodbank.enumerator.RoleType;
import net.mezan.foodbank.service.UserService;
import net.mezan.foodbank.util.URL;
import net.mezan.foodbank.util.View;
import net.mezan.foodbank.web.helper.CookieManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Date;
import java.util.Locale;


@Controller
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationService authService;

    @Autowired
    ActivityLogService activityLogService;

    @Autowired
    CookieManager cookieManager;

    @GetMapping(value = URL.ADMIN_LOGIN)
    public String showLoginSignup(Locale locale,
                                  HttpServletRequest request,
                                  Model model) {

        User user = cookieManager.getUserByValidatingCookie(request.getCookies());
        if (user != null) {
            AuthHelper.setSession(request, user);
            return "redirect:" + URL.ADMIN_DASHBOARD;
        }

        AuthHelper.bindLoginFormData(model, new LoginForm());
        return View.ADMIN_LOGIN;
    }

    @PostMapping(value = URL.ADMIN_LOGIN)
    public String doLogin(Locale locale,
                          HttpServletRequest request,
                          HttpServletResponse response,
                          @Valid LoginForm loginForm,
                          BindingResult result,
                          Model model) {

        if (!result.hasErrors()) {

            if (authService.isAuthorized(loginForm)) {
                if (loginForm.isRememberMe()) {
                    Cookie cookie = cookieManager.setAuthCookie(loginForm.getEmail());
                    cookie.setPath(request.getContextPath());
                    response.addCookie(cookie);
                }

                User user = userService.getUser(loginForm.getEmail());

                if (AuthHelper.isUserHavingRole(user, RoleType.ADMIN)) {
                    AuthHelper.setSession(request, user);
                    activityLogService.writeLog(ActivityType.LOGIN, "You have logged in to your account", new Date()
                            , AuthHelper.getUserIdFromSession(request));
                    return "redirect:" + URL.ADMIN_DASHBOARD;
                }
            }

            model.addAttribute("loginFail", true);
        }

        AuthHelper.bindLoginFormData(model, loginForm);
        return View.ADMIN_LOGIN;
    }

    @GetMapping(value = URL.ADMIN_LOGOUT)
    @PostMapping(value = URL.ADMIN_LOGOUT)
    public String doLogOut(Locale locale,
                           HttpServletRequest request,
                           Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        if (userId > 0) {
            request.getSession().invalidate();
            cookieManager.invalidateAuthCookie(userId, request.getCookies());
        }

        activityLogService.writeLog(ActivityType.LOGOUT, "You have logged out to your account", new Date()
                , AuthHelper.getUserIdFromSession(request));
        return "redirect:" + URL.ADMIN_LOGIN;
    }
}