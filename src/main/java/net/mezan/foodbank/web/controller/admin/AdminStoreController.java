package net.mezan.foodbank.web.controller.admin;

import net.mezan.foodbank.domain.Store;
import net.mezan.foodbank.service.ActivityLogService;
import net.mezan.foodbank.service.admin.AdminStoreService;
import net.mezan.foodbank.util.URL;
import net.mezan.foodbank.util.View;
import net.mezan.foodbank.web.helper.AuthHelper;
import net.mezan.foodbank.enumerator.ActivityType;
import net.mezan.foodbank.service.admin.AdminProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Locale;


@Controller
public class AdminStoreController {

    @Autowired
    AdminStoreService adminStoreService;

    @Autowired
    AdminProductService adminProductService;

    @Autowired
    ActivityLogService activityLogService;

    @GetMapping(value = URL.ADMIN_STORES)
    public String getStores(Locale locale,
                            Model model) {

        model.addAttribute("storeList", adminStoreService.getStoreList());
        return View.ADMIN_STORES;
    }

    @GetMapping(value = URL.ADMIN_STORE)
    public String getStore(Locale locale,
                           @PathVariable(value = "id") int id,
                           @ModelAttribute Store store,
                           Model model) {

        model.addAttribute("store", adminStoreService.getStore(store));
        model.addAttribute("storeProducts", adminProductService.getProducts(id));
        return View.ADMIN_STORE_DETAILS;
    }

    @PostMapping(value = URL.ADMIN_VERIFY_STORE)
    public String verifyStore(Locale locale,
                              HttpServletRequest request,
                              @PathVariable(value = "id") int id,
                              @ModelAttribute Store store,
                              Model model) {

        adminStoreService.verifyStore(store);
        activityLogService.writeLog(ActivityType.UPDATE, "You have verified an store", new Date()
                , AuthHelper.getUserIdFromSession(request));

        return "redirect:" + URL.ADMIN_STORE;
    }
}