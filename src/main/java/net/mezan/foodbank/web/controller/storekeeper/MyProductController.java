package net.mezan.foodbank.web.controller.storekeeper;

import net.mezan.foodbank.domain.*;
import net.mezan.foodbank.service.ActivityLogService;
import net.mezan.foodbank.service.storekeeper.*;
import net.mezan.foodbank.enumerator.ActivityType;
import net.mezan.foodbank.enumerator.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;


@Controller
public class MyProductController {

    @Autowired
    MyProductService myProductService;

    @Autowired
    MyBookService myBookService;

    @Autowired
    MyPublisherService myPublisherService;

    @Autowired
    MyAuthorService myAuthorService;

    @Autowired
    MyStoreService myStoreService;

    @Autowired
    ActivityLogService activityLogService;

    @GetMapping(value = "/mystores/{store_id}/products")
    public String getProducts(Locale locale, Model model,
                              @PathVariable(value = "store_id") long store_id,
                              HttpSession session) {
        try {
            if (myStoreService.getStore(store_id).getStatus() != Status.APPROVED ||
                    Long.parseLong(session.getAttribute("USER_ID").toString()) != myStoreService.getUserID(store_id)) {
                session.setAttribute("isStoreOwner", "false");
            } else {
                session.setAttribute("isStoreOwner", "true");
            }

            model.addAttribute("productList", myProductService.getProducts(store_id));
        } catch (Exception e) {
            session.setAttribute("errorMsg", "Something went wrong, try again");
            return "redirect:/mystores";
        }
        return "store/myProducts";
    }

    @GetMapping(value = "/mystores/{store_id}/product/add")
    public String addProduct(Locale locale, Model model,
                             @PathVariable(value = "store_id") long store_id,
                             HttpSession session) {
        try {
            if (myStoreService.getStore(store_id).getStatus() != Status.APPROVED ||
                    Long.parseLong(session.getAttribute("USER_ID").toString()) != myStoreService.getUserID(store_id)) {
                return "redirect:/mystores";
            }

            model.addAttribute("bookList", myBookService.getBooks());
        } catch (Exception e) {
            session.setAttribute("errorMsg", "Something went wrong, try again");
            return "redirect:/mystores/{store_id}/products";
        }
        return "store/addProduct";
    }

    @GetMapping(value = "/mystores/{store_id}/product/add/book")
    public String addBook(Locale locale, Model model,
                          @RequestParam("selectedBook") String book_id,
                          @PathVariable(value = "store_id") long store_id,
                          HttpSession session) {
        try {
            if (myStoreService.getStore(store_id).getStatus() != Status.APPROVED ||
                    Long.parseLong(session.getAttribute("USER_ID").toString()) != myStoreService.getUserID(store_id)) {
                return "redirect:/mystores";
            }

            if (book_id.equals("otherBook")) {
                model.addAttribute("publisherList", myPublisherService.getPublishers());
                session.setAttribute("newBook", "true");
                return "store/addBook";
            }
            session.setAttribute("book1", Long.parseLong(book_id));
            session.setAttribute("newBook", null);
        } catch (Exception e) {
            session.setAttribute("errorMsg", "Something went wrong, try again");
            return "redirect:/mystores/{store_id}/products";
        }
        return "store/addProductFinal";
    }

    @GetMapping(value = "/mystores/{store_id}/product/add/publisher")
    public String addPublisher(Locale locale,
                               @PathVariable(value = "store_id") long store_id,
                               Model model,
                               @RequestParam("noOfAuthors") int noOfAuthors,
                               @RequestParam("selectedPublisher") String publisherId, HttpSession session) {
        try {
            if (myStoreService.getStore(store_id).getStatus() != Status.APPROVED ||
                    Long.parseLong(session.getAttribute("USER_ID").toString()) != myStoreService.getUserID(store_id)) {
                return "redirect:/mystores";
            }

            session.setAttribute("noOfAuthors", noOfAuthors);
            model.addAttribute("authorList", myAuthorService.getAuthors());
            if (publisherId.equals("otherPublisher")) {
                session.setAttribute("newPublisher", "true");
                return "store/addPublisher";
            }
            session.setAttribute("publisher1", Long.parseLong(publisherId));
        } catch (Exception e) {
            session.setAttribute("errorMsg", "Something went wrong, try again");
            return "redirect:/mystores/{store_id}/products";
        }
        return "store/addAuthor";
    }

    @GetMapping(value = "/mystores/{store_id}/product/add/author")
    public String addAuthor(Locale locale, Model model,
                            @Valid @ModelAttribute Publisher publisher,
                            BindingResult result,
                            @PathVariable(value = "store_id") long store_id,
                            HttpSession session) {
        try {
            if (myStoreService.getStore(store_id).getStatus() != Status.APPROVED ||
                    Long.parseLong(session.getAttribute("USER_ID").toString()) != myStoreService.getUserID(store_id)) {
                return "redirect:/mystores";
            }

            model.addAttribute("authorList", myAuthorService.getAuthors());
            if (session.getAttribute("newPublisher") != null) {
                session.setAttribute("publisher1", myPublisherService.addPublisher(publisher).getId());
                activityLogService.writeLog(ActivityType.INSERT, "You have added a new Publisher "
                                + publisher.getName(), new Date()
                        , Long.parseLong(session.getAttribute("USER_ID").toString()));
                session.setAttribute("newPublisher", null);
            }
        } catch (Exception e) {
            model.addAttribute("authorList", myAuthorService.getAuthors());
            session.setAttribute("errorMsg", "Publisher Already Exists");
            return "store/addPublisher";
        }
        return "store/addAuthor";
    }

    @GetMapping(value = "/mystores/{store_id}/product/add/bookFinal")
    public String addBookFinal(Locale locale, Model model,
                               @PathVariable(value = "store_id") long store_id,
                               @RequestParam Map<String, String> params, HttpSession session) {
        try {
            if (myStoreService.getStore(store_id).getStatus() != Status.APPROVED
                    || Long.parseLong(session.getAttribute("USER_ID").toString()) != myStoreService.getUserID(store_id)) {
                return "redirect:/mystores";
            }


            List<Long> selectedAuthorIndexes = new ArrayList<>();

            for (int i = 1; i <= Integer.parseInt(session.getAttribute("noOfAuthors").toString()); i++) {
                if (params.get("selectedAuthor" + i).equals("otherAuthor")) {
                    session.setAttribute("selectedAuthor" + i, null);
                    selectedAuthorIndexes.add(-1L);
                } else {
                    session.setAttribute("selectedAuthor" + i, "true");
                    session.setAttribute("author" + i, Long.parseLong(params.get("selectedAuthor" + i)));
                    selectedAuthorIndexes.add(Long.parseLong(params.get("selectedAuthor" + i)));
                }
            }

            model.addAttribute("selectedAuthorList", myAuthorService.getSelectedAuthors(selectedAuthorIndexes));
        } catch (Exception e) {
            session.setAttribute("errorMsg", "Something went wrong, try again");
            return "redirect:/mystores/{store_id}/products";
        }
        return "store/addBookFinal";
    }

    @GetMapping(value = "/mystores/{store_id}/product/add/productFinal")
    public String addAll(Locale locale, Model model,
                         @PathVariable(value = "store_id") long store_id,
                         @RequestParam Map<String, String> params,
                         HttpSession session) {
        try {
            if (myStoreService.getStore(store_id).getStatus() != Status.APPROVED
                    || Long.parseLong(session.getAttribute("USER_ID").toString())
                    != myStoreService.getUserID(store_id)) {
                return "redirect:/mystores";
            }

            for (int authorNo = 1; authorNo <= Integer.parseInt(session.getAttribute("noOfAuthors").toString());
                 authorNo++) {
                if (session.getAttribute("selectedAuthor" + authorNo) == null) {
                    Author author = new Author();
                    author.setName(params.get("name" + authorNo));
                    author.setDetails(params.get("details" + authorNo));
                    author.setImageUrl(params.get("imageUrl" + authorNo));
                    session.setAttribute("author" + authorNo, myAuthorService.addAuthor(author).getId());
                    activityLogService.writeLog(ActivityType.INSERT, "You have added a new Author "
                                    + author.getName(), new Date()
                            , Long.parseLong(session.getAttribute("USER_ID").toString()));
                }
            }

            if (session.getAttribute("newBook") != null) {
                Book book = new Book();
                book.setCountry(params.get("country"));
                book.setEdition(params.get("edition"));
                book.setImageUrl(params.get("imageUrl"));
                book.setIsbn(params.get("isbn"));
                book.setLanguage(params.get("language"));
                book.setNoOfPages(Integer.parseInt(params.get("noOfPages")));
                book.setTitle(params.get("title"));
                book.setPublisher(myPublisherService.getPublisher(Long.parseLong(session.getAttribute("publisher1")
                        .toString())));

                List<Author> authorList = new ArrayList<>();

                for (int authorNo = 1; authorNo <= Integer.parseInt(session.getAttribute("noOfAuthors").toString());
                     authorNo++) {
                    authorList.add(myAuthorService.getAuthor(Long.parseLong(session.getAttribute("author" + authorNo)
                            .toString())));
                }
                book.setAuthorList(authorList);

                session.setAttribute("book1", myBookService.addBook(book).getId());
                activityLogService.writeLog(ActivityType.UPDATE, "You have added a new Book " + book.getTitle()
                        , new Date(), Long.parseLong(session.getAttribute("USER_ID").toString()));
            }
        } catch (Exception e) {
            session.setAttribute("errorMsg", "Something went wrong, try again");
            return "redirect:/mystores/{store_id}/products";
        }
        return "store/addProductFinal";
    }

    @PostMapping(value = "/mystores/{store_id}/product/add/productFinal")
    public String addProductFinal(Locale locale, Model model,
                                  @PathVariable(value = "store_id") long store_id,
                                  @RequestParam Map<String, String> params,
                                  HttpSession session) {

        try {
            if (myStoreService.getStore(store_id).getStatus() != Status.APPROVED
                    || Long.parseLong(session.getAttribute("USER_ID").toString())
                    != myStoreService.getUserID(store_id)) {
                return "redirect:/mystores";
            }

            Product product = new Product();
            product.setAvgRating(0);
            product.setBasePrice(Integer.parseInt(params.get("basePrice")));
            product.setBook(myBookService.getBook(Long.parseLong(session.getAttribute("book1").toString())));
            product.setCartList(null);
            product.setDiscount(Integer.parseInt(params.get("discount")));
            product.setIsVerified(0);
            product.setOrderedProductList(null);
            product.setPurchasePrice(Integer.parseInt(params.get("purchasePrice")));
            product.setStockQuantity(Integer.parseInt(params.get("stockQuantity")));
            product.setStore(myStoreService.getStore(store_id));

            model.addAttribute("storeList",
                    myStoreService.getMyStores(Long.parseLong(session.getAttribute("USER_ID").toString())));
            if (myStoreService.getUserID(store_id) == Long.parseLong(session.getAttribute("USER_ID").toString())) {
                myProductService.addProduct(product);
                session.setAttribute("successMsg", "Product Added");
                activityLogService.writeLog(ActivityType.INSERT, "You have Added a new Product : "
                                + product.getBook().getTitle() + " (price : " + product.getBasePrice() + " taka)"
                        , new Date(), Long.parseLong(session.getAttribute("USER_ID").toString()));
            }

        } catch (Exception e) {
            session.setAttribute("errorMsg", "Something went wrong, try again");
            return "redirect:/mystores/{store_id}/products";
        }
        return "redirect:/mystores/" + store_id + "/products";
    }

    @GetMapping(value = "/mystores/{store_id}/product/{product_id}/update")
    public String updateProduct(Locale locale, Model model,
                                @PathVariable(value = "store_id") long store_id,
                                @PathVariable(value = "product_id") long product_id,
                                HttpSession session) {
        try {
            if (Long.parseLong(session.getAttribute("USER_ID").toString()) != myStoreService.getUserID(store_id)) {
                return "redirect:/mystores";
            }
            Product product = myProductService.getProduct(product_id);
            model.addAttribute(product);
            Book book = product.getBook();
            session.setAttribute("BOOK_ID", book.getId());
        } catch (Exception e) {
            session.setAttribute("errorMsg", "Something went wrong, try again");
            return "redirect:/mystores/{store_id}/products";
        }
        return "store/updateProduct";
    }

    @PostMapping(value = "/mystores/{store_id}/product/{product_id}/update")
    public String updateProduct(Locale locale, HttpSession session,
                                @Valid @ModelAttribute Product product, Model model,
                                @PathVariable(value = "store_id") long store_id,
                                BindingResult result) {
        try {
            if (Long.parseLong(session.getAttribute("USER_ID").toString()) != myStoreService.getUserID(store_id)) {
                return "redirect:/mystores";
            }

            Store store = myStoreService.getStore(store_id);
            product.setStore(store);
            Book book = myBookService.getBook(Long.parseLong(session.getAttribute("BOOK_ID").toString()));
            product.setBook(book);
            myProductService.updateProduct(product);
            activityLogService.writeLog(ActivityType.UPDATE, "You have Updated the Information of the Product : "
                            + product.getBook().getTitle() + " (price : " + product.getBasePrice() + " taka)"
                    , new Date(), Long.parseLong(session.getAttribute("USER_ID").toString()));
            session.setAttribute("successMsg", "Product Updated");
            model.addAttribute("storeList", myStoreService.getMyStores(Long.parseLong(session.getAttribute("USER_ID")
                    .toString())));
            session.setAttribute("logMsg", "Product Updated");
        } catch (Exception e) {
            session.setAttribute("errorMsg", "Something went wrong, try again");
            return "redirect:/mystores/{store_id}/products";
        }
        return "redirect:/mystores/" + store_id + "/products";
    }

    @PostMapping(value = "/mystores/{store_id}/product/{product_id}/delete")
    public String deleteProduct(Locale locale, HttpSession session, @PathVariable(value = "store_id") long store_id
            , @PathVariable(value = "product_id") long product_id) {
        try {
            if (Long.parseLong(session.getAttribute("USER_ID").toString()) != myStoreService.getUserID(store_id)) {
                return "redirect:/mystores";
            }
            Product product = myProductService.getProduct(product_id);
            myProductService.deleteProduct(product);
            activityLogService.writeLog(ActivityType.DELETE, "You have Deleted the Product : "
                            + product.getBook().getTitle() + " (price : " + product.getBasePrice() + " taka)"
                    , new Date(), Long.parseLong(session.getAttribute("USER_ID").toString()));
            session.setAttribute("successMsg", "Product Deleted");
        } catch (Exception e) {
            session.setAttribute("errorMsg", "You can't delete the product, there is some customer orders " +
                    "associated with it");
            return "redirect:/mystores/{store_id}/products";
        }
        return "redirect:/mystores/" + store_id + "/products";
    }
}
