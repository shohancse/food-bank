package net.mezan.foodbank.web.controller.admin;

import net.mezan.foodbank.service.admin.AdminDashboardService;
import net.mezan.foodbank.service.admin.AdminOrderService;
import net.mezan.foodbank.service.admin.AdminProductService;
import net.mezan.foodbank.service.admin.AdminStoreService;
import net.mezan.foodbank.util.URL;
import net.mezan.foodbank.util.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Locale;


@Controller
public class AdminDashboardController {

    @Autowired
    AdminDashboardService adminDashboardService;

    @Autowired
    AdminOrderService adminOrderService;

    @Autowired
    AdminProductService adminProductService;

    @Autowired
    AdminStoreService adminStoreService;

    @GetMapping(value = URL.ADMIN_DASHBOARD)
    public String getDashboardInfo(Locale locale,
                                   Model model) {

        model.addAttribute("countPendingOrders", adminOrderService.countPendingOrders());
        model.addAttribute("countProcessingOrders", adminOrderService.countProcessingOrders());
        model.addAttribute("countNotVerifiedProducts", adminProductService.countNotVerifiedProducts());
        model.addAttribute("countPendingStores", adminStoreService.countPendingStores());
        model.addAttribute("userStatistics", adminDashboardService.getUserStatistics());
        model.addAttribute("bestSellerStatistics", adminDashboardService.getBestSellerStatistics());
        return View.ADMIN_DASHBOARD;
    }
}