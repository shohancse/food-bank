package net.mezan.foodbank.web.controller;

import net.mezan.foodbank.domain.Store;
import net.mezan.foodbank.service.admin.AdminStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@Controller
public class StoreViewController {

    @Autowired
    private AdminStoreService storeService;

    @GetMapping(value = "/stores")
    public String getStores(Model model, HttpServletRequest request) {

        int pageNumber = 1;
        int pageSize = 6;

        if (request.getParameter("page") != null) {
            pageNumber = Integer.parseInt(request.getParameter("page"));
        }

        long recordsCount = storeService.findCount();
        long noOfPages = (int) Math.ceil(recordsCount * 1.0 / pageSize);
        List<Store> storeList = storeService.getStores(pageNumber - 1, pageSize);

        model.addAttribute("noOfPages", noOfPages);
        model.addAttribute("currentPage", pageNumber);
        model.addAttribute("viewList", storeList);
        model.addAttribute("viewIcon", "storeicon.jpg");
        model.addAttribute("viewType", "Store");
        model.addAttribute("viewUrl", "stores");

        return "navigation_view";
    }

    @GetMapping(value = "/stores/{id}")
    public String storeProducts(Model model, @PathVariable(value = "id") long storeId) {
        return "redirect:/product/store/" + storeId;
    }

    @GetMapping(value="/stores/search")
    public String storeSearch(Model model, @RequestParam("searchKeyWord")String searchKeyWord) {

        String pageToLoad = "navigation_view";
        if (searchKeyWord.equals("")) {
            pageToLoad = "redirect:/stores";
        }
        else {

            List<Store> storeList = new ArrayList<>();
            storeList.add(storeService.getSearchResult(formatSearchKeyWord(searchKeyWord)));

            model.addAttribute("noOfPages", 1);
            model.addAttribute("currentPage", 1);
            model.addAttribute("viewList", storeList);
            model.addAttribute("viewIcon", "storeicon.jpg");
            model.addAttribute("viewType", "Store");
            model.addAttribute("viewUrl", "stores");
        }

        return pageToLoad;
    }
    private String formatSearchKeyWord(String searchKeyWord) {
        return searchKeyWord.substring(0, 1).toUpperCase() + searchKeyWord.substring(1).toLowerCase();
    }
}
