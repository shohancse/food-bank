package net.mezan.foodbank.web.controller;

import net.mezan.foodbank.domain.Publisher;
import net.mezan.foodbank.service.publisher.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@Controller
public class PublisherViewController {

    @Autowired
    private PublisherService publisherService;

    @GetMapping(value = "/publishers")
    public String getCategories(Model model, HttpServletRequest request) {

        int pageNumber = 1;
        int pageSize = 6;

        if (request.getParameter("page") != null) {
            pageNumber = Integer.parseInt(request.getParameter("page"));
        }

        long recordsCount = publisherService.findCount();
        long noOfPages = (int) Math.ceil(recordsCount * 1.0 / pageSize);
        List<Publisher> publisherList = publisherService.getPublishers(pageNumber - 1, pageSize);

        model.addAttribute("noOfPages", noOfPages);
        model.addAttribute("currentPage", pageNumber);
        model.addAttribute("viewList", publisherList);
        model.addAttribute("viewIcon", "publishericon.jpg");
        model.addAttribute("viewType", "Publisher");
        model.addAttribute("viewUrl", "publishers");

        return "navigation_view";
    }

    @GetMapping(value = "/publishers/{id}")
    public String publisherProduct(Model model, @PathVariable(value = "id") long publisherId) {
        return "redirect:/product/publisher/" + publisherId;
    }

    @GetMapping(value="/publishers/search")
    public String publisherSearch(Model model, @RequestParam("searchKeyWord")String searchKeyWord) {

        String pageToLoad = "navigation_view";
        if (searchKeyWord.equals("")) {
            pageToLoad = "redirect:/publishers";
        }
        else {

            List<Publisher> publisherList = new ArrayList<>();
            publisherList.add(publisherService.getSearchResult(formatSearchKeyWord(searchKeyWord)));

            model.addAttribute("noOfPages", 1);
            model.addAttribute("currentPage", 1);
            model.addAttribute("viewList", publisherList);
            model.addAttribute("viewIcon", "publishericon.jpg");
            model.addAttribute("viewType", "Publisher");
            model.addAttribute("viewUrl", "publishers");
        }

        return pageToLoad;
    }
    private String formatSearchKeyWord(String searchKeyWord) {
        return searchKeyWord.substring(0, 1).toUpperCase() + searchKeyWord.substring(1).toLowerCase();
    }
}
