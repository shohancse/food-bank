package net.mezan.foodbank.web.controller.admin;

import net.mezan.foodbank.service.ActivityLogService;
import net.mezan.foodbank.web.helper.AuthHelper;
import net.mezan.foodbank.domain.Product;
import net.mezan.foodbank.enumerator.ActivityType;
import net.mezan.foodbank.service.admin.AdminProductService;
import net.mezan.foodbank.util.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Locale;


@Controller
public class AdminProductVerificationController {

    @Autowired
    AdminProductService adminProductService;

    @Autowired
    ActivityLogService activityLogService;

    @GetMapping(value = URL.ADMIN_NEW_PRODUCTS)
    public String getNewProducts(Locale locale,
                                 Model model) {

        model.addAttribute("newProductList", adminProductService.getNewProducts());
        return URL.ADMIN_NEW_PRODUCTS;
    }

    @PostMapping(value = URL.ADMIN_VERIFY_NEW_PRODUCT)
    public String verifyProduct(Locale locale,
                                HttpServletRequest request,
                                @ModelAttribute Product product,
                                Model model) {

        adminProductService.verifyNewProduct(product);
        activityLogService.writeLog(ActivityType.UPDATE, "You have verified a product", new Date()
                , AuthHelper.getUserIdFromSession(request));

        return URL.ADMIN_NEW_PRODUCTS;
    }
}