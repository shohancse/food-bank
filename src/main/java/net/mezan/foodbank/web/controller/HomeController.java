package net.mezan.foodbank.web.controller;

import net.mezan.foodbank.dao.StoreDao;
import net.mezan.foodbank.domain.Store;
import net.mezan.foodbank.service.admin.AdminStoreService;
import net.mezan.foodbank.service.category.UserCategoryService;
import net.mezan.foodbank.service.product.CustomerProductService;
import net.mezan.foodbank.util.Constant;
import net.mezan.foodbank.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
public class HomeController {

    @Autowired
    private UserCategoryService userCategoryService;

    @Autowired
    private CustomerProductService productService;

    @Autowired
    private StoreDao storeDao;



    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model, HttpServletRequest request) {

        return "home";
    }

    @RequestMapping(value = "/restaurant", method = RequestMethod.GET)
    public String showRestaurant(String location, ModelMap model, HttpServletRequest request) {

        List<Store> storeList = storeDao.findAllBy("location", location);

        model.addAttribute("viewList", storeList);
        model.addAttribute("viewIcon", "storeicon.png");
        model.addAttribute("viewType", "Store");
        model.addAttribute("viewUrl", "stores");

        return "navigation_view";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String showSearchResult(@RequestParam("property") String property, @RequestParam("searchKeyWord")
                                    String searchKeyWord, Model model) {
        String pageToLoad;
        if (searchKeyWord.equals("")) {
            pageToLoad = "redirect:/home";

        } else {
            model.addAttribute("productList", productService.getSearchResult(property,
                    formatSearchKeyWord(searchKeyWord)));

            pageToLoad = "books";
        }

        return pageToLoad;
    }

    private String formatSearchKeyWord(String searchKeyWord) {
        return searchKeyWord.substring(0, 1).toUpperCase() + searchKeyWord.substring(1).toLowerCase();
    }
}