package net.mezan.foodbank.web.controller;

import net.mezan.foodbank.domain.Author;
import net.mezan.foodbank.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@Controller
public class AuthorViewController {

    @Autowired
    private AuthorService authorService;

    @GetMapping(value = "/authors")
    public String getAuthors(Model model, HttpServletRequest request) {

        int pageNumber = 1;
        int pageSize = 6;

        if (request.getParameter("page") != null) {
            pageNumber = Integer.parseInt(request.getParameter("page"));
        }

        long recordsCount = authorService.findCount();
        long noOfPages = (int) Math.ceil(recordsCount * 1.0 / pageSize);
        List<Author> authorList = authorService.getAuthors(pageNumber - 1, pageSize);

        model.addAttribute("noOfPages", noOfPages);
        model.addAttribute("currentPage", pageNumber);
        model.addAttribute("viewList", authorList);
        model.addAttribute("viewIcon", "authoricon.jpg");
        model.addAttribute("viewType", "Author");
        model.addAttribute("viewUrl", "authors");

        return "navigation_view";
    }

    @GetMapping(value = "/authors/{id}")
    public String authorProduct(Model model, @PathVariable(value = "id") long authorId) {
        return "redirect:/product/author/" + authorId;
    }

    @GetMapping(value="/authors/search")
    public String authorSearch(Model model, @RequestParam("searchKeyWord")String searchKeyWord, HttpServletRequest request) {

        String pageToLoad = "navigation_view";
        if (searchKeyWord.equals("")) {
            pageToLoad = "redirect:/authors";
        }
        else {

            List<Author> authorList = new ArrayList<>();
            authorList.add(authorService.getSearchResult(formatSearchKeyWord(searchKeyWord)));

            model.addAttribute("noOfPages", 1);
            model.addAttribute("currentPage", 1);
            model.addAttribute("viewList", authorList);
            model.addAttribute("viewIcon", "authoricon.jpg");
            model.addAttribute("viewType", "Author");
            model.addAttribute("viewUrl", "authors");
        }

        return pageToLoad;
    }
    private String formatSearchKeyWord(String searchKeyWord) {
        return searchKeyWord.substring(0, 1).toUpperCase() + searchKeyWord.substring(1).toLowerCase();
    }
}
