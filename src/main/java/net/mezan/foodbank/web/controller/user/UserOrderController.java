package net.mezan.foodbank.web.controller.user;

import net.mezan.foodbank.service.UserOrderService;
import net.mezan.foodbank.service.UserService;
import net.mezan.foodbank.util.URL;
import net.mezan.foodbank.util.View;
import net.mezan.foodbank.web.command.OrderInfo;
import net.mezan.foodbank.web.helper.AuthHelper;
import net.mezan.foodbank.web.helper.UserHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;


@Controller
public class UserOrderController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserOrderService userOrderService;

    @GetMapping(value = URL.USER_ORDERS)
    public String showUserOrders(Locale locale,
                                 HttpServletRequest request,
                                 @PathVariable int page,
                                 @PathVariable int size,
                                 Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        List<OrderInfo> orderInfoList = userOrderService.getUserOrderInfoList(userId, page, size);

        model.addAttribute("currentPage", page);
        model.addAttribute("noOfPages", (userOrderService.getTotalOrderCount(userId) / size));
        model.addAttribute("orderList", orderInfoList);
        model.addAttribute("size", size);
        UserHelper.bindUserCard(userService, request, model);
        return View.USER_ORDERS;
    }

    @GetMapping(value = URL.USER_ORDER_INVOICE)
    public String showUserOrderInvoice(Locale locale,
                                       HttpServletRequest request,
                                       @PathVariable long orderId,
                                       Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        OrderInfo orderInfo = userOrderService.getUserOrderInfo(userId, orderId);
        if (orderInfo != null) {

            model.addAttribute("order", orderInfo);
            UserHelper.bindUserCard(userService, request, model);
            return View.USER_ORDER_INVOICE;
        }

        return View.NOT_FOUND;
    }
}
