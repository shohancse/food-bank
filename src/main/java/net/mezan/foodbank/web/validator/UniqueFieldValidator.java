package net.mezan.foodbank.web.validator;

import net.mezan.foodbank.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


@Component
public class UniqueFieldValidator implements ConstraintValidator<UniqueField, Object> {

    @Autowired
    private UserService userService;

    private String fieldName;

    @Override
    public void initialize(UniqueField constraintAnnotation) {
        fieldName = constraintAnnotation.fieldName();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {

        switch (fieldName) {
            case "email":
                return userService.getUser((String) value) == null;
            default:
                return false;
        }
    }
}
