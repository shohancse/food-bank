package net.mezan.foodbank.web.helper;

import net.mezan.foodbank.domain.AuthToken;
import net.mezan.foodbank.domain.User;
import net.mezan.foodbank.service.AuthorizationService;
import net.mezan.foodbank.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;


@Component
public class CookieManager {

    public static final String AUTH_COOKIE_NAME = "AUTHENTICATION_VALUE";
    public static final int COOKIE_AGE = 24 * 60 * 60 * 1000;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationService authService;

    public User getUserByValidatingCookie(Cookie[] cookies) {

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(AUTH_COOKIE_NAME)) {

                User user = authService.getUserByAuthCookie(cookie.getValue());
                if (user != null) {

                    return user;
                }
            }
        }

        return null;
    }

    public Cookie setAuthCookie(String userEmail) {

        AuthToken token = authService.setAuthToken(userEmail);
        Cookie cookie = new Cookie(AUTH_COOKIE_NAME, token.getToken());
        cookie.setMaxAge((int) token.getExpiresOn().getTime());
        return cookie;
    }

    public Cookie invalidateAuthCookie(long userId, Cookie[] cookies) {

        authService.removeAllAuthTokens(userId);

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(AUTH_COOKIE_NAME)) {

                cookie.setMaxAge(0);
                cookie.setValue(null);
                return cookie;
            }
        }

        return null;
    }
}
