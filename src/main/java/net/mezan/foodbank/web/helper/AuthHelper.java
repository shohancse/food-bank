package net.mezan.foodbank.web.helper;

import net.mezan.foodbank.web.command.LoginForm;
import net.mezan.foodbank.domain.Role;
import net.mezan.foodbank.domain.User;
import net.mezan.foodbank.enumerator.RoleType;
import net.mezan.foodbank.web.command.SignUpForm;
import org.springframework.ui.Model;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class AuthHelper {

    public static Model bindLoginFormData(Model model, LoginForm loginForm) {

        loginForm.setPassword("");
        model.addAttribute("loginForm", loginForm);
        return model;
    }

    public static Model bindSignUpFormData(Model model, SignUpForm signUpForm) {

        signUpForm.setPassword("");
        signUpForm.setVerifyPassword("");
        model.addAttribute("signUpForm", signUpForm);
        return model;
    }

    public static void setSession(HttpServletRequest request, User user) {
        HttpSession session = request.getSession();
        session.setAttribute("USER_ID", user.getId());
        session.setAttribute("ROLE_LIST", user.getUserRoleString());
    }

    public static long getUserIdFromSession(HttpServletRequest request) {

        try {
            HttpSession session = request.getSession();
            return (long) session.getAttribute("USER_ID");
        } catch (NullPointerException ignore) {
        }

        return 0;
    }

    public static boolean isUserHavingRole(User user, RoleType roleType) {

        for (Role role : user.getRoleList()) {
            if (role.getName().equals(roleType)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isUserHavingRole(ServletRequest req, RoleType roleType) {

        HttpServletRequest request = (HttpServletRequest) req;
        String roleList = (String) request.getSession().getAttribute("ROLE_LIST");

        if (roleList.contains(roleType.toString())){
            return true;
        }

        return false;
    }
}
