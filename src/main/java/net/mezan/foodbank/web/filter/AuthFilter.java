package net.mezan.foodbank.web.filter;

import net.mezan.foodbank.web.helper.AuthHelper;
import net.mezan.foodbank.enumerator.RoleType;
import net.mezan.foodbank.service.UserService;
import net.mezan.foodbank.util.URL;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebFilter({"/user/*", "/admin/*", "/mystores/*", "/cart/*"})
public class AuthFilter implements Filter {

    @Autowired
    UserService userService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String path = httpRequest.getServletPath();

        httpResponse.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        httpResponse.setHeader("Pragma", "no-cache");


        long userId = AuthHelper.getUserIdFromSession(httpRequest);

        if (isPathInvalid(userId, path)) {

            if (AuthHelper.isUserHavingRole(request, RoleType.ADMIN) && path.contains("admin")) {
                httpResponse.sendRedirect(httpRequest.getContextPath() + URL.ADMIN_DASHBOARD);
            } else {
                httpResponse.sendRedirect(httpRequest.getContextPath() + URL.HOME);
            }

            return;
        }

        if (isExcludedFromFilter(path)) {
            chain.doFilter(request, response);

        } else {

            if (userId > 0) {
                if (!AuthHelper.isUserHavingRole(request, RoleType.ADMIN) && path.contains("admin")) {
                    httpResponse.sendRedirect(httpRequest.getContextPath() + URL.USER_LOGIN);
                }
                chain.doFilter(request, response);
            } else {
                if (path.contains("admin")) {
                    httpResponse.sendRedirect(httpRequest.getContextPath() + URL.ADMIN_LOGIN);
                } else {
                    httpResponse.sendRedirect(httpRequest.getContextPath() + URL.USER_LOGIN);
                }
            }
        }
    }

    public boolean isPathInvalid(long userId, String path) {
        return userId > 0 &&
                (path.startsWith(URL.USER_LOGIN)
                        || path.startsWith(URL.USER_SIGN_UP)
                        || path.startsWith(URL.ADMIN_LOGIN)
                        || path.startsWith(URL.FORGOT_PASSWORD)
                        || path.startsWith(URL.RESET_PASSWORD));
    }

    private boolean isExcludedFromFilter(String path) {
        return path.startsWith(URL.USER_LOGIN)
                || path.startsWith(URL.USER_SIGN_UP)
                || path.startsWith(URL.VERIFY_EMAIL_PREFIX)
                || path.startsWith(URL.ADMIN_LOGIN)
                || path.startsWith(URL.FORGOT_PASSWORD)
                || path.startsWith(URL.RESET_PASSWORD)
                || path.startsWith(URL.RESOURCES_PREFIX);
    }

    @Override
    public void destroy() {
    }
}
