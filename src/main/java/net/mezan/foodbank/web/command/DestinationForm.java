package net.mezan.foodbank.web.command;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import java.io.Serializable;


public class DestinationForm implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "Address can't be empty!!")
    private String address;

    @NotBlank(message = "City can't be empty!!")
    private String city;

    @NotBlank(message = "Post Code can't be empty!!")
    @Pattern(regexp = "[0-9]+", message = "Invalid Post Code!!")
    private String postCode;

    @NotBlank(message = "Country can't be empty!!")
    private String country;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return address + ", "
                + "Post Code: "
                + postCode + ", "
                + city + ", "
                + country;

    }
}
