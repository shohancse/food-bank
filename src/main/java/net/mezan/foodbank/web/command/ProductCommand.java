package net.mezan.foodbank.web.command;

import net.mezan.foodbank.domain.Category;
import net.mezan.foodbank.domain.Product;

import java.util.List;


public class ProductCommand {

    private Product product;

    private Category category;

    private String authors;

    private double currentPrice;

    private List<Product> listOfProductsFromSimilarAuthor;

    private List<Product> listOfProductsFromSimilarPublisher;

    private List<Product> listOfProductsFromSimilarStore;

    private List<Product> listOfProductsFromSimilarCategory;

    private List<Product> listOfProductsFromSimilarUsers;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Product> getListOfProductsFromSimilarAuthor() {
        return listOfProductsFromSimilarAuthor;
    }

    public void setListOfProductsFromSimilarAuthor(List<Product> listOfProductsFromSimilarAuthor) {
        this.listOfProductsFromSimilarAuthor = listOfProductsFromSimilarAuthor;
    }

    public List<Product> getListOfProductsFromSimilarPublisher() {
        return listOfProductsFromSimilarPublisher;
    }

    public void setListOfProductsFromSimilarPublisher(List<Product> listOfProductsFromSimilarPublisher) {
        this.listOfProductsFromSimilarPublisher = listOfProductsFromSimilarPublisher;
    }

    public List<Product> getListOfProductsFromSimilarStore() {
        return listOfProductsFromSimilarStore;
    }

    public void setListOfProductsFromSimilarStore(List<Product> listOfProductsFromSimilarStore) {
        this.listOfProductsFromSimilarStore = listOfProductsFromSimilarStore;
    }

    public List<Product> getListOfProductsFromSimilarCategory() {
        return listOfProductsFromSimilarCategory;
    }

    public void setListOfProductsFromSimilarCategory(List<Product> listOfProductsFromSimilarCategory) {
        this.listOfProductsFromSimilarCategory = listOfProductsFromSimilarCategory;
    }

    public List<Product> getListOfProductsFromSimilarUsers() {
        return listOfProductsFromSimilarUsers;
    }

    public void setListOfProductsFromSimilarUsers(List<Product> listOfProductsFromSimilarUsers) {
        this.listOfProductsFromSimilarUsers = listOfProductsFromSimilarUsers;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }
}