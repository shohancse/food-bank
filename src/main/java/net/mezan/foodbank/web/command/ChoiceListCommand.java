package net.mezan.foodbank.web.command;

import java.util.List;


public class ChoiceListCommand {

    private List<Long> authorIds;
    private List<Long> categoryIds;
    private List<Long> publisherIds;
    private List<Long> storeIds;

    public List<Long> getAuthorIds() {
        return authorIds;
    }

    public void setAuthorIds(List<Long> authorIds) {
        this.authorIds = authorIds;
    }

    public List<Long> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<Long> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public List<Long> getPublisherIds() {
        return publisherIds;
    }

    public void setPublisherIds(List<Long> publisherIds) {
        this.publisherIds = publisherIds;
    }

    public List<Long> getStoreIds() {
        return storeIds;
    }

    public void setStoreIds(List<Long> storeIds) {
        this.storeIds = storeIds;
    }
}
