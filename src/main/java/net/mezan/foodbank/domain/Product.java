package net.mezan.foodbank.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;



@Entity
@Table(name = "product")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "store_id")
    private Store store;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    @OneToMany(mappedBy = "product")
    private List<SaleLog> saleLogList;

    @OneToMany(mappedBy = "product")
    private List<UserProductRating> userProductRatingList;

    @OneToMany(mappedBy = "product")
    private List<Cart> cartList;

    @OneToMany(mappedBy = "product")
    private List<OrderedProduct> orderedProductList;

    @Column(name = "rating_count")
    private int ratingCount;

    @Column(name = "avg_rating")
    private double avgRating;

    @Column(name = "purchase_price")
    private double purchasePrice;

    @Column(name = "base_price")
    private double basePrice;
    private double discount;

    @Column(name = "stock_quantity")
    private int stockQuantity;

    @Column(name = "is_verified")
    private int isVerified;

    public Product() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public List<SaleLog> getSaleLogList() {
        return saleLogList;
    }

    public void setSaleLogList(List<SaleLog> saleLogList) {
        this.saleLogList = saleLogList;
    }

    public List<UserProductRating> getUserProductRatingList() {
        return userProductRatingList;
    }

    public void setUserProductRatingList(List<UserProductRating> userProductRatingList) {
        this.userProductRatingList = userProductRatingList;
    }

    public List<Cart> getCartList() {
        return cartList;
    }

    public void setCartList(List<Cart> cartList) {
        this.cartList = cartList;
    }

    public List<OrderedProduct> getOrderedProductList() {
        return orderedProductList;
    }

    public void setOrderedProductList(List<OrderedProduct> orderedProductList) {
        this.orderedProductList = orderedProductList;
    }

    public int getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }

    public double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(double avgRating) {
        this.avgRating = avgRating;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public int getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(int isVerified) {
        this.isVerified = isVerified;
    }
}
