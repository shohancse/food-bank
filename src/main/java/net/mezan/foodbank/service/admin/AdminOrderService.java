package net.mezan.foodbank.service.admin;

import net.mezan.foodbank.dao.SaleLogDao;
import net.mezan.foodbank.dao.OrderDao;
import net.mezan.foodbank.dao.ProductDao;
import net.mezan.foodbank.domain.Order;
import net.mezan.foodbank.domain.OrderedProduct;
import net.mezan.foodbank.domain.Product;
import net.mezan.foodbank.domain.SaleLog;
import net.mezan.foodbank.enumerator.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;


@Service
@Transactional
public class AdminOrderService {

    @Autowired
    OrderDao orderDao;

    @Autowired
    SaleLogDao saleLogDao;

    @Autowired
    ProductDao productDao;

    public List<Order> getPendingOrders() {

        List<Order> orderList = orderDao.findAllBy("orderStatus", OrderStatus.PENDING);
        for (Order order : orderList) {
            order.getOrderedProductList().size();
        }

        return sortOrderList(orderList);
    }

    public List<Order> getProcessingOrders() {

        List<Order> orderList = orderDao.findAllBy("orderStatus", OrderStatus.PROCESSING);
        for (Order order : orderList) {
            order.getOrderedProductList().size();
        }

        return sortOrderList(orderList);
    }

    public void processOrder(Order order) {

        order = orderDao.find(order.getId());
        order.setOrderStatus(OrderStatus.PROCESSING);
        orderDao.update(order);

        for (OrderedProduct orderedProduct : order.getOrderedProductList()) {
            Product product = productDao.find(orderedProduct.getProduct().getId());
            product.setStockQuantity(product.getStockQuantity() - orderedProduct.getQuantity());
            productDao.save(product);
        }
    }

    public void deliverOrder(Order order, Date deliveryDate) {

        order = orderDao.find(order.getId());
        order.setOrderStatus(OrderStatus.DELIVERED);
        order.setDeliveryDate(deliveryDate);
        orderDao.update(order);

        for (OrderedProduct orderedProduct : order.getOrderedProductList()) {
            SaleLog saleLog = new SaleLog();
            saleLog.setDate(order.getDeliveryDate());
            saleLog.setQuantity(orderedProduct.getQuantity());
            saleLog.setSalePrice(order.getTotalPrice());
            saleLog.setProduct(orderedProduct.getProduct());
            saleLog.setUser(order.getUser());
            saleLogDao.save(saleLog);
        }

    }

    public Order getOrder(Order order) {

        order = orderDao.find(order.getId());
        order.getOrderedProductList().size();
        return order;
    }

    public long countPendingOrders() {
        return orderDao.findAllBy("orderStatus", OrderStatus.PENDING).size();
    }

    public long countProcessingOrders() {
        return orderDao.findAllBy("orderStatus", OrderStatus.PROCESSING).size();
    }

    public void discardOrder(Order order) {
        order = orderDao.find(order.getId());
        order.setOrderStatus(OrderStatus.DISCARDED);
        orderDao.update(order);
    }

    public List<Order> sortOrderList(List<Order> orderList) {

        List<Order> sortedOrderList = new ArrayList<>(orderList);
        Collections.sort(sortedOrderList, new myComparator());
        return sortedOrderList;
    }

    public class myComparator implements Comparator<Order> {
        @Override
        public int compare(Order order1, Order order2) {
            return order1.getCheckoutTime().compareTo(order2.getCheckoutTime());
        }
    }
}