package net.mezan.foodbank.service.admin;

import net.mezan.foodbank.dao.SaleLogDao;
import net.mezan.foodbank.web.command.BestSellarCommand;
import net.mezan.foodbank.dao.AuthTokenDao;
import net.mezan.foodbank.dao.OrderDao;
import net.mezan.foodbank.web.command.UserStatisticsCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AdminDashboardService {

    @Autowired
    OrderDao orderDao;

    @Autowired
    SaleLogDao saleLogDao;

    @Autowired
    AuthTokenDao authTokenDao;

    public List<UserStatisticsCommand> getUserStatistics() {
        return null;
    }

    public List<BestSellarCommand> getBestSellerStatistics() {
        return null;
    }
}