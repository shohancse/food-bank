package net.mezan.foodbank.service.admin;

import net.mezan.foodbank.dao.StoreDao;
import net.mezan.foodbank.domain.Store;
import net.mezan.foodbank.dao.AuthorDao;
import net.mezan.foodbank.dao.ProductDao;
import net.mezan.foodbank.enumerator.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service(value = "adminStoreService")
@Transactional
public class AdminStoreService {

    @Autowired
    StoreDao storeDao;

    @Autowired
    ProductDao productDao;

    @Autowired
    AuthorDao authorDao;

    public List<Store> getStoreList() {
        return storeDao.findAllStoreOrderByStatus();
    }

    public void verifyStore(Store store) {

        Status status = store.getStatus();
        store = storeDao.find(store.getId());
        store.setStatus(status);
        storeDao.update(store);
    }

    public Store getStore(Store store) {
        return storeDao.find(store.getId());
    }

    public List<Store> getStores(int pageNumber, int pageSize) {
        return storeDao.findAll(pageNumber, pageSize);
    }

    public Long findCount() {
        return storeDao.findCount();
    }

    public long countPendingStores() {
        return storeDao.findAllBy("status", Status.PENDING).size();
    }

    public Store getSearchResult(String searchKeyWord) {

        return storeDao.doesExist(searchKeyWord);
    }
}