package net.mezan.foodbank.service;

import net.mezan.foodbank.dao.UserDao;
import net.mezan.foodbank.domain.User;
import net.mezan.foodbank.util.EncryptionUtil;
import net.mezan.foodbank.web.command.UserProfileForm;
import net.mezan.foodbank.web.command.SignUpForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;


@Transactional
@Service(value = "userService")
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleService roleService;

    public User getUser(long id) {
        User user = userDao.findBy("id", id);
        if(user != null){
            user.getRoleList().size();
        }

        return user;
    }

    public User getUser(String userEmail) {
        User user = userDao.findBy("email", userEmail);
        if(user != null){
            user.getRoleList().size();
        }
        return user;
    }

    public void signUpUser(SignUpForm signUpForm) {

        User user = new User();
        user.setFirstName(signUpForm.getFirstName());
        user.setLastName(signUpForm.getLastName());
        user.setEmail(signUpForm.getEmail());
        user.setAddress(signUpForm.getAddress());
        user.setDateOfBirth(signUpForm.getDateOfBirth());
        user.setPhone(signUpForm.getPhone());
        user.setGender(signUpForm.getGender());
        user.setPassword(EncryptionUtil.generateSecureHash(signUpForm.getPassword()));
        user.setApproved(false);
        user.setCreatedAt(new Date());

        user.setRoleList(roleService.getUserRoleList());

        userDao.save(user);
    }

    public void updateUser(long id, UserProfileForm userProfile) {

        User user = userDao.find(id);
        user.setFirstName(userProfile.getFirstName());
        user.setLastName(userProfile.getLastName());
        user.setGender(userProfile.getGender());
        user.setDateOfBirth(userProfile.getDateOfBirth());
        user.setPhone(userProfile.getPhone());
        user.setAddress(userProfile.getAddress());
        user.setBio(userProfile.getBio());

        userDao.save(user);
    }

    public void insertUser(User user) {
        userDao.save(user);
    }
}
