package net.mezan.foodbank.service.storekeeper;

import net.mezan.foodbank.dao.MyOrderDao;
import net.mezan.foodbank.dao.UserDao;
import net.mezan.foodbank.domain.OrderedProduct;
import net.mezan.foodbank.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class MyOrderService {

    @Autowired
    MyOrderDao myOrderDao;

    @Autowired
    UserDao userDao;

    public List<OrderedProduct> getMyOrders(User user, int page, int size) {
        return myOrderDao.getMyOrders(userDao.find(user.getId()), page, size);
    }

    public long countMyNextOrders(User user, int page, int size) {
        return myOrderDao.getMyOrders(userDao.find(user.getId()), page, size).size();
    }
}
