package net.mezan.foodbank.service.storekeeper;

import net.mezan.foodbank.dao.StoreDao;
import net.mezan.foodbank.domain.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class MyStoreService {

    @Autowired
    StoreDao storeDao;

    public List<Store> getMyStores(long id) {
        return storeDao.findAllBySorted("user_id", id);
    }

    public Store getStore(long store_id) {
        return storeDao.findBy("id", store_id);
    }

    public void addStore(Store store) {
        storeDao.save(store);
    }

    public void updateStore(Store store) {
        storeDao.update(store);
    }

    public void deleteStore(Store store) {
        storeDao.deleteDetached(store);
    }

    public List<Store> getStoreList() {
        return storeDao.findAll();
    }

    public void verifyStore(Store store) {
        storeDao.update(store);
    }

    public Store getStore(Store store) {
        return storeDao.find(store.getId());
    }

    public List<Store> getPopularStores() {
        return storeDao.getPopularStores();
    }

    public long getUserID(long store_id) {
        return storeDao.findBy("id", store_id).getUser().getId();
    }
}