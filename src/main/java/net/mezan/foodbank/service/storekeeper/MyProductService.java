package net.mezan.foodbank.service.storekeeper;

import net.mezan.foodbank.dao.ProductDao;
import net.mezan.foodbank.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class MyProductService {

    @Autowired
    ProductDao productDao;

    public List<Product> getProducts(long store_id) {
        return productDao.findAllBySorted("store_id", store_id);
    }

    public void addProduct(Product product) {
        productDao.save(product);
    }

    public void updateProduct(Product product) {
        productDao.update(product);
    }

    public void deleteProduct(Product product) {
        productDao.deleteDetached(product);
    }

    public Product getProduct(long product_id) {
        return productDao.findBy("id", product_id);
    }
}
