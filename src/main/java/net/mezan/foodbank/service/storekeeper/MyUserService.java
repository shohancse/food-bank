package net.mezan.foodbank.service.storekeeper;

import net.mezan.foodbank.domain.User;
import net.mezan.foodbank.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
@Transactional
public class MyUserService {

    @Autowired
    UserDao userDao;

    public User getUser(long id) {
        return userDao.findBy("id", id);
    }
}
