package net.mezan.foodbank.service.storekeeper;

import net.mezan.foodbank.dao.PublisherDao;
import net.mezan.foodbank.domain.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class MyPublisherService {

    @Autowired
    PublisherDao publisherDao;

    public List<Publisher> getPublishers() {
        return publisherDao.findAll();
    }

    public Publisher getPublisher(long id) {
        return publisherDao.findBy("id", id);
    }

    public Publisher addPublisher(Publisher publisher) {
        return publisherDao.addPublisher(publisher);
    }
}
