package net.mezan.foodbank.service.storekeeper;

import net.mezan.foodbank.dao.BookDao;
import net.mezan.foodbank.domain.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class MyBookService {

    @Autowired
    BookDao bookDao;

    public Book addBook(Book book) {
        return bookDao.addBook(book);
    }

    public List<Book> getBooks() {
        return bookDao.findAll();
    }

    public Book getBook(long id) {
        return bookDao.findBy("id", id);
    }
}