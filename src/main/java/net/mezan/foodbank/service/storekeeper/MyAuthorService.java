package net.mezan.foodbank.service.storekeeper;

import net.mezan.foodbank.dao.AuthorDao;
import net.mezan.foodbank.domain.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class MyAuthorService {
    @Autowired
    AuthorDao authorDao;

    public List<Author> getAuthors() {
        return authorDao.findAll();
    }

    public Author getAuthor(long id) {
        return authorDao.findBy("id", id);
    }

    public Author addAuthor(Author author) {
        return authorDao.addAuthor(author);
    }

    public List<Author> getSelectedAuthors(List<Long> selectedAuthorIndexes) {
        List<Author> authorList = new ArrayList<>();
        for (Long selectedAuthorIndex : selectedAuthorIndexes) {
            if (selectedAuthorIndex == 0L) {
                Author author = new Author();
                author.setImageUrl("1");
                authorList.add(author);
            } else {
                authorList.add(authorDao.findBy("id", selectedAuthorIndex));
            }
        }
        return authorList;
    }
}
