package net.mezan.foodbank.service;

import net.mezan.foodbank.dao.AuthorDao;
import net.mezan.foodbank.domain.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class AuthorService {

    @Autowired
    private AuthorDao authorDao;

    public List<Author> getAuthors() {
        return authorDao.findAll();
    }

    public List<Author> getPopularAuthors() {
        return authorDao.getPopularAuthors();
    }

    public List<Author> getAuthors(int pageNumber, int pageSize) {
        return authorDao.findAll(pageNumber, pageSize);
    }

    public Long findCount() {
        return authorDao.findCount();
    }

    public Author getSearchResult(String searchKeyWord) {

        return authorDao.doesExist(searchKeyWord);
    }
}