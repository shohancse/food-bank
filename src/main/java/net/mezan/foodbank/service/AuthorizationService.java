package net.mezan.foodbank.service;

import net.mezan.foodbank.dao.VerificationTokenDao;
import net.mezan.foodbank.domain.AuthToken;
import net.mezan.foodbank.domain.User;
import net.mezan.foodbank.util.EncryptionUtil;
import net.mezan.foodbank.web.command.LoginForm;
import net.mezan.foodbank.dao.AuthTokenDao;
import net.mezan.foodbank.dao.UserDao;
import net.mezan.foodbank.domain.VerificationToken;
import net.mezan.foodbank.web.helper.CookieManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;


@Transactional
@Service(value = "authService")
public class AuthorizationService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private AuthTokenDao authTokenDao;

    @Autowired
    private VerificationTokenDao verificationTokenDao;

    @Autowired
    private JavaMailSender mailSender;

    public boolean isAuthorized(LoginForm loginForm) {
        User user = userDao.findBy("email", loginForm.getEmail());
        return user != null &&
                EncryptionUtil.matchWithSecureHash(loginForm.getPassword(), user.getPassword());
    }

    public User getUserByAuthCookie(String authCookie) {
        AuthToken authToken = authTokenDao.findBy("token", authCookie);
        if (authToken != null) {
            return authToken.getExpiresOn().getTime() > new Date().getTime() ?
                    authToken.getUser() : null;
        }
        return null;
    }

    public void sendEmailVerificationLink(String userEmail) {
        User user = userDao.findBy("email", userEmail);
        if (user != null && !user.isApproved()) {

            String token = EncryptionUtil.generateSecureHash(user.toString());
            verificationTokenDao.deleteBy("user", user);

            VerificationToken verificationToken = new VerificationToken();
            verificationToken.setToken(token);
            verificationToken.setExpiresOn(new Date(new Date().getTime() + 24 * 60 * 60 * 1000));
            verificationToken.setUsed(false);
            verificationToken.setUser(user);

            verificationTokenDao.save(verificationToken);

            SimpleMailMessage email = new SimpleMailMessage();
            email.setTo(user.getEmail());
            email.setSubject("Verify Email For foodbank");

            StringBuffer sb = new StringBuffer();
            sb.append("Hello,").append("\n");
            sb.append("An Account has been created with you Email Address. " +
                    "To verify your email follow the link.").append("\n")
                    .append("http://localhost:8080/foodbank/user/verify/")
                    .append(userEmail).append("/")
                    .append(token);

            email.setText(sb.toString());
            mailSender.send(email);
        }
    }

    public boolean isVerificationTokenValid(String userEmail, String token) {
        User user = userDao.findBy("email", userEmail);
        if (user != null && !user.isApproved()) {

            VerificationToken verificationToken = verificationTokenDao.findBy("user", user);
            if (!verificationToken.isUsed() && verificationToken.getExpiresOn().getTime() > new Date().getTime()) {

                if (verificationToken.getToken().equals(token)) {

                    user.setApproved(true);
                    verificationToken.setUsed(true);

                    userDao.save(user);
                    verificationTokenDao.save(verificationToken);
                    return true;
                }
            }
        }

        return false;
    }

    public AuthToken setAuthToken(String userEmail) {
        AuthToken authToken = null;
        User user = userDao.findBy("email", userEmail);
        if (user != null) {

            String token = EncryptionUtil.generateSecureHash(user.getEmail());
            authToken = new AuthToken();
            authToken.setToken(token);
            authToken.setExpiresOn(new Date(new Date().getTime() + CookieManager.COOKIE_AGE));
            authToken.setUser(user);

            authTokenDao.save(authToken);
        }

        return authToken;
    }

    public void removeAllAuthTokens(long userId) {
        User user = userDao.find(userId);
        if (user != null) {

            List<AuthToken> tokenList = authTokenDao.findAllBy("user", user);
            for (AuthToken token : tokenList) {
                authTokenDao.delete(token);
            }
        }
    }
}
