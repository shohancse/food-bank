package net.mezan.foodbank.service;

import net.mezan.foodbank.dao.ActivityLogDao;
import net.mezan.foodbank.domain.ActivityLog;
import net.mezan.foodbank.dao.UserDao;
import net.mezan.foodbank.domain.User;
import net.mezan.foodbank.enumerator.ActivityType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;


@Transactional
@Service("activityLogService")
public class ActivityLogService {

    @Autowired
    ActivityLogDao activityLogDao;

    @Autowired
    UserDao userDao;

    public List<ActivityLog> getActivityLog(User user, int page, int size) {
        return activityLogDao.getActivityLogs(userDao.find(user.getId()), page, size);
    }

    public long countNextLogs(User user, int page, int size) {
        return activityLogDao.findAllBy("user", user, page, size).size();
    }

    public void writeLog(ActivityType activityType, String message, Date date, long userId) {

        ActivityLog activityLog = new ActivityLog();
        activityLog.setActivityType(activityType);
        activityLog.setCreatedAt(new Date());
        activityLog.setMessage(message);
        activityLog.setUser(userDao.find(userId));
        activityLogDao.save(activityLog);
    }

    public List<ActivityLog> getUserActivityLog(long userId, int page, int size) {
        User user = userDao.findBy("id", userId);
        return activityLogDao.getActivityLogs(user, page, size);
    }

    public void insertActivityLog(long userId, ActivityType type, String message) {
        User user = userDao.find(userId);
        if (user != null) {

            ActivityLog log = new ActivityLog();
            log.setActivityType(type);
            log.setCreatedAt(new Date());
            log.setMessage(user.getFirstName()+" "+user.getLastName()+" "+message);
            log.setUser(user);

            activityLogDao.save(log);
        }
    }

    public List<ActivityLog> getMyActivityLog(User user, int page, int size) {
        return activityLogDao.getActivityLogs(userDao.find(user.getId()), page, size);
    }

    public long countMyNextLogs(User user, int page, int size) {
        return activityLogDao.findAllBy("user", user, page, size).size();
    }
}