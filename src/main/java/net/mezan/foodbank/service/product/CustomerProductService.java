package net.mezan.foodbank.service.product;

import net.mezan.foodbank.dao.*;
import net.mezan.foodbank.domain.*;
import net.mezan.foodbank.web.command.ChoiceListCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class CustomerProductService {

    @Autowired
    private ProductDao productDao;

    @Autowired
    private PublisherDao publisherDao;

    @Autowired
    private StoreDao storeDao;

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private AuthorDao authorDao;

    public List<Product> getProductsByTopRating(String avgRating, double ratingValue) {

        return productDao.findObjectsGreaterAndAbove(avgRating, ratingValue);
    }

    public List<Product> getSearchResult(String property, String searchKeyWord) {

        List<Product> searchresult = new ArrayList<>();

        if (property.equals("Category")) {

            Category category = categoryDao.doesExist(searchKeyWord);
            if (category != null) {
                searchresult = productDao.getProductsByCategory(category.getId());
            }

        } else if (property.equals("Author")) {

            Author author = authorDao.doesExist(searchKeyWord);
            if (author != null) {
                searchresult = productDao.getProductsByAuthor(author.getId());
            }

        } else if (property.equals("Publisher")) {

            Publisher publisher = publisherDao.doesExist(searchKeyWord);
            if (publisher != null) {
                searchresult = productDao.getProductsByPublisher(publisher.getId());
            }

        } else if (property.equals("Store")) {

            Store store = storeDao.doesExist(searchKeyWord);
            if (store != null) {
                searchresult = productDao.getProductsByStores(store);
            }

        }

        return searchresult;
    }

    public Product getProduct(long id){
        return productDao.find(id);
    }

    public List<Product> getProductsbyCategory(long id) {
        return productDao.getProductsByCategory(id);
    }

    public List<Product> getProductsbyPublisher(long id) {
        return productDao.getProductsByPublisher(id);
    }

    public List<Product> getProductsbyStore(Store store) {
        return productDao.getProductsByStores(store);
    }

    public List<Product> getProductsbyAuthor(long id) {
        return productDao.getProductsByAuthor(id);
    }

    public List<Author> getAuthor(Product product){
        return product.getBook().getAuthorList();
    }

    public Category getCategory(Product product){
        return product.getBook().getSubCategoryList().get(0).getCategory();
    }

    public Publisher getPublisher(Product product){
        return product.getBook().getPublisher();
    }

    public Store getStore(Product product){
        return product.getStore();
    }

    public List<Product> getProductsByAuthor(long authorId) {
        return productDao.getProductsByAuthor(authorId);
    }

    public List<Product> getProductsbByPublisher(long publisherId) {
        return productDao.getProductsByPublisher(publisherId);
    }

    public List<Product> getProductsByStore(long storeId) {
        Store store = new Store();
        store.setId(storeId);
        return productDao.getProductsByStores(store);
    }

    public List<Product> getFilteredProducts(ChoiceListCommand choiceListCommand, String specificType, Long specificTypeId) {

        return productDao.filterBy(choiceListCommand, specificType, specificTypeId);
    }

    public List<Product> getProducts(int pageNumber, int pageSize, String fieldName
            ,Object threshold) {
        return productDao.findTopProductsPaginated(pageNumber, pageSize, fieldName, threshold);
    }
}