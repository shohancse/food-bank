package net.mezan.foodbank.service;

import net.mezan.foodbank.util.DateUtil;
import net.mezan.foodbank.domain.Order;
import net.mezan.foodbank.domain.OrderedProduct;
import net.mezan.foodbank.domain.Product;
import net.mezan.foodbank.domain.User;
import net.mezan.foodbank.web.command.OrderInfo;
import net.mezan.foodbank.web.command.ProductInfo;

import java.util.ArrayList;
import java.util.List;


public class UserServiceHelper {

    public static OrderInfo getOrderInfoFromEntity(Order order) {

        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderId(order.getId());
        orderInfo.setCheckoutDate(DateUtil.formatDate(order.getCheckoutTime(), "dd, MMM, YYYY"));
        orderInfo.setDeliveryDate(DateUtil.formatDate(order.getDeliveryDate(), "dd, MMM, YYYY"));
        orderInfo.setPaymentInfo(order.getPaymentOption().toString());
        orderInfo.setOrderStatus(order.getOrderStatus().toString());

        User user = order.getUser();
        StringBuilder userAddress = new StringBuilder();
        userAddress.append(user.getFirstName()).append(" ")
                .append(user.getLastName()).append("\n")
                .append(user.getAddress()).append("\n")
                .append(user.getPhone());

        orderInfo.setBilledTo(userAddress.toString());
        orderInfo.setShippingAddress(order.getDestination());

        orderInfo.setSubTotalPrice(order.getTotalPrice() + order.getShippingCost());
        orderInfo.setTotalPrice(order.getTotalPrice());
        orderInfo.setShippingCost(order.getShippingCost());

        orderInfo.setOrderStatus(order.getOrderStatus().toString());

        List<ProductInfo> productInfoList = new ArrayList<>();
        for (OrderedProduct orderedProduct : order.getOrderedProductList()) {
            productInfoList.add(getProductInfoFromEntity(orderedProduct));
        }

        orderInfo.setProductList(productInfoList);
        return orderInfo;
    }

    public static ProductInfo getProductInfoFromEntity(OrderedProduct orderedProduct) {
        ProductInfo productInfo = new ProductInfo();
        productInfo.setQuantity(orderedProduct.getQuantity());
        productInfo.setPrice(orderedProduct.getSubTotalPrice() / orderedProduct.getQuantity());
        productInfo.setSubTotal(orderedProduct.getSubTotalPrice());

        Product product = orderedProduct.getProduct();
        mapProductInfoFromEntity(productInfo, product);

        return productInfo;
    }

    public static ProductInfo mapProductInfoFromEntity(ProductInfo productInfo, Product product) {
        productInfo.setProductId(product.getId());
        productInfo.setAuthor(product.getBook().getAuthorString());
        productInfo.setAvgRating(product.getAvgRating());
        productInfo.setBookTitle(product.getBook().getTitle());
        productInfo.setProductPhoto(product.getBook().getImageUrl());

        return productInfo;
    }
}
