package net.mezan.foodbank.service.cart;

import net.mezan.foodbank.dao.CartDao;
import net.mezan.foodbank.domain.*;
import net.mezan.foodbank.enumerator.OrderStatus;
import net.mezan.foodbank.dao.OrderDao;
import net.mezan.foodbank.dao.ProductDao;
import net.mezan.foodbank.enumerator.PaymentOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;


@Service
@Transactional
public class CartService {

    public static final double SHIPPING_COST = 30.0;

    @Autowired
    CartDao cartDao;
    @Autowired
    OrderDao orderDao;
    @Autowired
    ProductDao productDao;

    public void addToCart(long userId, long productId) {
        List<Cart> cartList = cartDao.findAllBy("user_id", userId);
        if (cartList.size() > 0) {
            for (Cart cart : cartList) {
                if (cart.getProduct().getId() == productId) {
                    cart.setSubTotalPrice((cart.getSubTotalPrice() / cart.getQuantity()) *
                            (cart.getQuantity() + 1));
                    cart.setQuantity(cart.getQuantity() + 1);

                    return;
                }
            }
        }

        Product product = productDao.find(productId);
        User user = new User();
        user.setId(userId);

        Cart cart = new Cart();
        cart.setProduct(product);
        cart.setUser(user);

        double discount = product.getBasePrice() * (product.getDiscount() / 100.0);
        double sellingPrice = product.getBasePrice() - discount;
        cart.setSubTotalPrice(cart.getQuantity() * sellingPrice);

        cartDao.save(cart);
    }

    public void updateCart(long cartId, int quantity) {
        Cart cart = cartDao.find(cartId);
        cart.setQuantity(quantity);

        double discount = cart.getProduct().getBasePrice() * (cart.getProduct().getDiscount() / 100.0);
        double sellingPrice = cart.getProduct().getBasePrice() - discount;
        cart.setSubTotalPrice(cart.getQuantity() * sellingPrice);
    }

    public void deleteCart(long cartId) {
        cartDao.delete(cartId);
    }

    public void deleteCartBy(long userId) {
        cartDao.deleteBy("user_id", userId);
    }

    public long saveOrder(String paymentOption, String bkashTransactionId, String destination, long userId) {
        Order order = new Order();

        if (paymentOption.equals("bkash")) {
            order.setPaymentOption(PaymentOption.BKASH);
            order.setBkashTransactionId(bkashTransactionId);
        } else {
            order.setPaymentOption(PaymentOption.CASH_ON_DELIVERY);
        }

        User user = new User();
        user.setId(userId);

        order.setUser(user);
        order.setDestination(destination);
        order.setCheckoutTime(new Date());
        order.setOrderStatus(OrderStatus.PENDING);
        order.setShippingCost(SHIPPING_COST);

        List<Cart> cartList = cartDao.findAllBy("user_id", userId);
        double totalPrice = 0.0;
        for (Cart cart : cartList) {
            OrderedProduct orderedProduct = new OrderedProduct();
            orderedProduct.setOrder(order);
            orderedProduct.setQuantity(cart.getQuantity());
            orderedProduct.setProduct(cart.getProduct());
            orderedProduct.setSubTotalPrice(cart.getSubTotalPrice());

            order.getOrderedProductList().add(orderedProduct);

            totalPrice += cart.getSubTotalPrice();
        }

        order.setTotalPrice(totalPrice + SHIPPING_COST);
        orderDao.save(order);
        orderDao.flushEntityManager();

        return order.getId();
    }
}
