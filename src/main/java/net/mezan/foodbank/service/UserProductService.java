package net.mezan.foodbank.service;

import net.mezan.foodbank.dao.OrderedProductDao;
import net.mezan.foodbank.domain.User;
import net.mezan.foodbank.web.command.ProductInfo;
import net.mezan.foodbank.dao.UserDao;
import net.mezan.foodbank.dao.UserProductRatingDao;
import net.mezan.foodbank.domain.OrderedProduct;
import net.mezan.foodbank.domain.Product;
import net.mezan.foodbank.domain.UserProductRating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Transactional
@Service(value = "userProductService")
public class UserProductService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserProductRatingDao userProductRatingDao;

    @Autowired
    private OrderedProductDao orderedProductDao;

    public List<ProductInfo> getUserPurchasedProducts(long userId) {
        List<ProductInfo> productInfoList = new ArrayList<>();
        User user = userDao.find(userId);
        if (user != null) {

            List<OrderedProduct> productList = orderedProductDao.findAllUserPurchasedProduct(user);
            for (OrderedProduct orderedProduct : productList) {
                productInfoList.add(UserServiceHelper.getProductInfoFromEntity(orderedProduct));
            }
        }

        return productInfoList;
    }

    public List<ProductInfo> getUserRatedProducts(long userId) {
        List<ProductInfo> productInfoList = new ArrayList<>();
        User user = userDao.find(userId);
        if (user != null) {

            List<UserProductRating> ratedProductList = userProductRatingDao.findAllBy("user", user);
            for (UserProductRating ratedProduct : ratedProductList) {

                ProductInfo productInfo = new ProductInfo();
                Product product = ratedProduct.getProduct();
                productInfo = UserServiceHelper.mapProductInfoFromEntity(productInfo, product);
                productInfo.setAvgRating(ratedProduct.getRatingValue());

                productInfoList.add(productInfo);
            }
        }

        return productInfoList;
    }
}
