package net.mezan.foodbank.service;

import net.mezan.foodbank.dao.UserProductRatingDao;
import net.mezan.foodbank.domain.UserProductRating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserProductRatingService {

    @Autowired
    private UserProductRatingDao userProductRatingDao;

    public void save(UserProductRating userProductRating) {
        userProductRatingDao.save(userProductRating);
    }
}
