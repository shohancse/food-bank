package net.mezan.foodbank.service.category;

import net.mezan.foodbank.dao.CategoryDao;
import net.mezan.foodbank.domain.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class UserCategoryService {

    @Autowired
    private CategoryDao categoryDao;

    public List<Category> getPopularCategories() {

        return categoryDao.getPopularCategories();
    }
}