package net.mezan.foodbank.service.publisher;

import net.mezan.foodbank.dao.PublisherDao;
import net.mezan.foodbank.domain.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class PublisherService {

    @Autowired
    private PublisherDao publisherDao;

    public List<Publisher> getPopularPublishers() {
        return publisherDao.getPopularPublishers();
    }

    public List<Publisher> getPublishers(int pageNumber, int pageSize) {
        return publisherDao.findAll(pageNumber, pageSize);
    }

    public List<Publisher> getPublishers() {
        return publisherDao.findAll();
    }

    public Long findCount() {
        return publisherDao.findCount();
    }
    public Publisher getSearchResult(String searchKeyWord) {

        return publisherDao.doesExist(searchKeyWord);
    }
}

