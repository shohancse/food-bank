package net.mezan.foodbank.enumerator;


public enum ActivityType {
    UPDATE, INSERT, DELETE, PURCHASE, SIGNUP, EMAIL_VERIFICATION, PASSWORD_CHANGE,
    PROFILE_UPDATE, PASSWORD_RECOVERY, LOGIN, LOGOUT, REVIEW
}
