package net.mezan.foodbank.enumerator;


public enum OrderStatus {
    PENDING, PROCESSING, DELIVERED, DISCARDED
}