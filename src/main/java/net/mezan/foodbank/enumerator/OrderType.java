package net.mezan.foodbank.enumerator;


public enum OrderType {
    PERSONAL, GIFT
}
