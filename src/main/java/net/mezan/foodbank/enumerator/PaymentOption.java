package net.mezan.foodbank.enumerator;


public enum PaymentOption {
    CASH_ON_DELIVERY, BKASH
}
