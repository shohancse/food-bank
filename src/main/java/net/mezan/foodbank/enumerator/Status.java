package net.mezan.foodbank.enumerator;


public enum Status {
    APPROVED, PENDING, REJECTED
}
