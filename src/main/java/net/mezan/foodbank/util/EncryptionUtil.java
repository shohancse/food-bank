package net.mezan.foodbank.util;

import org.mindrot.jbcrypt.BCrypt;


public class EncryptionUtil {

    public static String generateSecureHash(String originalString) {
        return BCrypt.hashpw(originalString, BCrypt.gensalt(12));
    }

    public static boolean matchWithSecureHash(String providedString, String existingHash) {
        return BCrypt.checkpw(providedString, existingHash);
    }
}
