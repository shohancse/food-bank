package net.mezan.foodbank.util;


public class Constant {

    public static final double THRESHOLD_RATING = 3.5;
    public static final String AVERAGE_RATING = "avg_rating";
}
