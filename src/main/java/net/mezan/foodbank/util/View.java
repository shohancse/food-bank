package net.mezan.foodbank.util;


public interface View {

    public static final String LOGIN_SIGNUP_VIEW = "user/loginSignup";
    public static final String VERIFIED_VIEW = "user/verified";
    public static final String FORGOT_PASSWORD_VIEW = "user/forgotPassword";

    public static final String USER_HOME = "user/userOverview";
    public static final String USER_PROFILE = "user/userProfile";
    public static final String USER_PROFILE_EDIT = "user/userProfileEdit";
    public static final String USER_ORDERS = "user/userOrders";
    public static final String USER_ORDER_INVOICE = "user/userOrderInvoice";
    public static final String USER_PURCHASES = "user/userPurchases";
    public static final String USER_RATINGS = "/user/userRatings";
    public static final String USER_ACTIVITY_LOG = "user/activityLog";

    public static final String NOT_FOUND = "404";

    public static final String ADMIN_DASHBOARD = "admin/dashboard";
    public static final String ADMIN_ACTIVITY = "admin/activity";
    public static final String ADMIN_PRODUCT_CATEGORY = "admin/category";
    public static final String ADMIN_PRODUCT_SUBCATEGORY = "admin/subcategory";
    public static final String ADMIN_LOGIN = "admin/login";
    public static final String ADMIN_ORDER_DETAILS = "admin/orderDetails";
    public static final String ADMIN_ORDERS = "admin/orders";
    public static final String ADMIN_STORES = "admin/stores";
    public static final String ADMIN_STORE_DETAILS = "admin/storeDetails";
    public static final String ADMIN_NEW_PRODUCTS= "admin/newProducts";


    public static final String CART = "cart/cart";
    public static final String CART_PAYMENT = "cart/payment";
    public static final String CART_SHIPPING = "cart/shipping";
    public static final String CART_EMPTY = "cart/emptycart";


}
