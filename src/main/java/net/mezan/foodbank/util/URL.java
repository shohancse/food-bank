package net.mezan.foodbank.util;


public interface URL {

    public static final String RESOURCES_PREFIX = "/resources";

    public static final String HOME = "/";

    public static final String USER_SIGN_UP = "/user/signup";
    public static final String USER_LOGIN = "/user/login";
    public static final String USER_LOGOUT = "/user/logout";

    public static final String VERIFY_EMAIL_PREFIX = "/user/verify";
    public static final String VERIFY_EMAIL = VERIFY_EMAIL_PREFIX + "/{email}/{token:.+}";
    public static final String RESEND_EMAIL_VERIFICATION = "/user/resend-verification-token/{email}";
    public static final String FORGOT_PASSWORD = "/user/forgot-password";
    public static final String RESET_PASSWORD = "/user/reset-password";
    public static final String CHANGE_PASSWORD = "/user/change-password";

    public static final String USER_OVERVIEW = "/user/overview";
    public static final String USER_PROFILE = "/user/profile";
    public static final String USER_PROFILE_EDIT = "/user/profile-edit";
    public static final String USER_ORDERS = "/user/orders/{page}/{size}";
    public static final String USER_ORDER_INVOICE = "/user/order-invoice/{orderId}";
    public static final String USER_PURCHASES = "/user/purchases";
    public static final String USER_RATINGS = "/user/my-ratings";
    public static final String USER_ACTIVITY_LOG = "/user/activity-log";

    public static final String ADMIN_LOGIN = "/admin/login";
    public static final String ADMIN_LOGOUT = "/admin/logout";

    public static final String ADMIN_DASHBOARD = "/admin/dashboard";
    public static final String ADMIN_ACTIVITY = "/admin/activities/{page}";
    public static final String ADMIN_CATEGORIES = "/admin/categories";
    public static final String ADMIN_ADD_CATEGORY = "/admin/category/add";
    public static final String ADMIN_UPDATE_CATEGORY = "/admin/category/update";
    public static final String ADMIN_SHOW_SUBCATEGORIES = "/admin/category/{categoryId}/subcategories";
    public static final String ADMIN_ADD_SUBCATEGORY = "/admin/category/{categoryId}/subcategory/add";
    public static final String ADMIN_UPDATE_SUBCATEGORY = "admin/category/{categoryId}/subcategory/update";
    public static final String ADMIN_ORDERS = "/admin/orders";
    public static final String ADMIN_ORDER_DETAILS = "/admin/order/{id}";
    public static final String ADMIN_PROCESS_ORDER = "/admin/order/{id}/process";
    public static final String ADMIN_DELIVER_ORDER = "/admin/order/{id}/deliver";
    public static final String ADMIN_DISCARD_ORDER = "/admin/order/{id}/discard";
    public static final String ADMIN_NEW_PRODUCTS = "/admin/newProducts";
    public static final String ADMIN_VERIFY_NEW_PRODUCT = "/admin/newProducts/verify";
    public static final String ADMIN_STORES = "/admin/stores";
    public static final String ADMIN_STORE = "/admin/store/{id}";
    public static final String ADMIN_VERIFY_STORE = "/admin/store/{id}/verify";


    public static final String CART_ADD = "/cart/add";
    public static final String CART_VIEW = "/cart/view";
    public static final String CART_EDIT = "/cart/edit";
    public static final String CART_REMOVE = "/cart/remove";
    public static final String CART_SHIPPING = "/cart/shipping";
    public static final String CART_SHIPPING_SAVE = "/cart/shipping/save";
    public static final String CART_SHIPPING_PAYMENT = "/cart/shipping/payment";
    public static final String CART_ORDER_SAVE = "/cart/order/save";
}
