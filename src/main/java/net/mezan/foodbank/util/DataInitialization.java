package net.mezan.foodbank.util;

import net.mezan.foodbank.domain.User;
import net.mezan.foodbank.enumerator.Gender;
import net.mezan.foodbank.service.RoleService;
import net.mezan.foodbank.service.UserService;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.Date;


public class DataInitialization {

    //@Autowired
    private UserService userService;

    //@Autowired
    private RoleService roleService;

    public void onApplicationEvent(ContextRefreshedEvent event) {
        String adminEmail = "flyleaf.store@gmail.com";
        String adminPassword = "therap";

        User user = userService.getUser(adminEmail);
        if (user == null) {

            user.setFirstName("Flyleaf");
            user.setLastName("Admin");
            user.setAddress("Dhaka");
            user.setGender(Gender.MALE);
            user.setDateOfBirth(new Date());
            user.setPhone("01710000000");
            user.setApproved(true);
            user.setCreatedAt(new Date());
            user.setPassword(EncryptionUtil.generateSecureHash(adminPassword));

            user.setRoleList(roleService.getAdminRoleList());

            userService.insertUser(user);
        }
    }
}
