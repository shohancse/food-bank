<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Foodbank</title>

    <link href="<c:url value="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" />"
          rel="stylesheet">


    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <style type="text/css">
        .table > tbody > tr > td, .table > tfoot > tr > td {
            vertical-align: middle;
        }

        @media screen and (max-width: 600px) {
            table#cart tbody td .form-control {
                width: 20%;
                display: inline !important;
            }

            .actions .btn {
                width: 36%;
                margin: 1.5em 0;
            }

            .actions .btn-info {
                float: left;
            }

            .actions .btn-danger {
                float: right;
            }

            table#cart thead {
                display: none;
            }

            table#cart tbody td {
                display: block;
                padding: .6rem;
                min-width: 320px;
            }

            table#cart tbody tr td:first-child {
                background: #333;
                color: #fff;
            }

            table#cart tbody td:before {
                content: attr(data-th);
                font-weight: bold;
                display: inline-block;
                width: 8rem;
            }

            table#cart tfoot td {
                display: block;
            }

            table#cart tfoot td .btn {
                display: block;
            }

        }
    </style>
</head>
<body>
<div class="container">
    <h3>Cart Summary</h3>
    <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Product</th>
            <th style="width:10%">Price</th>
            <th style="width:8%">Quantity</th>
            <th style="width:22%" class="text-center">Subtotal</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        <c:set var="total" value="${0.0}"/>
        <c:forEach var="cart" items="${cartList}">
            <tr>
                <td data-th="Product">
                    <div class="row">
                        <div class="col-sm-2 hidden-xs"><img
                                src="<c:out value="${cart.product.book.imageUrl}"> </c:out>" alt="..."
                                class="img-responsive"/></div>
                        <div class="col-sm-10">
                            <h4 class="nomargin"><a href="<c:url value="/product/${cart.product.id}"/>">
                                <c:out value="${cart.product.book.title}"></c:out></a></h4>

                            <p><c:out value="${cart.product.book.language}"></c:out></p>
                        </div>
                    </div>
                </td>
                <td data-th="Price">TK. <c:out value="${cart.product.basePrice}"></c:out><br>
                    <c:out value="(${cart.product.discount}% Discount)"></c:out></td>
                <td data-th="Quantity">
                    <c:out value="${cart.quantity}"></c:out>
                </td>
                <td data-th="Subtotal" class="text-center">TK. <c:out value="${cart.subTotalPrice}"></c:out></td>
                <c:set var="total" value="${total + cart.subTotalPrice}"/>
            </tr>
        </c:forEach>
        <tr>
            <td data-th="Product"></td>
            <td data-th="Price"></td>
            <td data-th="Quantity"></td>
            <td data-th="Subtotal" class="text-center"><strong>Total Payable TK. <c:out
                    value="${total}"></c:out></strong>
            </td>
        </tr>
        </tbody>
    </table>


    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <form:form class="form-horizontal" action="/foodbank/cart/shipping" commandName="destination">
                <fieldset>
                    <legend>Shipping Address</legend>

                    <div class="form-group">
                        <div class="col-sm-10">
                            <form:input path="address" placeholder="Address" class="form-control"/>
                        </div>

                    </div>
                    <div>
                        <form:errors path="address" cssClass="error"/>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-10">
                            <form:input path="city" placeholder="city" class="form-control"/>
                        </div>
                    </div>
                    <div>
                        <form:errors path="city"/>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-10">
                            <form:input path="postCode" placeholder="Post Code" class="form-control"/>
                        </div>
                    </div>
                    <div>
                        <form:errors path="postCode" cssClass="error"/>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-10">
                            <form:input path="country" placeholder="Country" class="form-control"/>
                        </div>
                    </div>
                    <div>
                        <form:errors path="country"/>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="pull-right">
                                <input type="submit" value="Continue" class="btn btn-primary"/>
                            </div>
                        </div>
                    </div>

                </fieldset>

            </form:form>
        </div>
    </div>
</div>

<script src="<c:url value="/resources/lib/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/lib/bootstrap/js/bootstrap.min.js" />"></script>

</body>
</html>