<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html lang="en">
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h3>Order Summary</h3>
    <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Product</th>
            <th style="width:10%">Price</th>
            <th style="width:8%">Quantity</th>
            <th style="width:22%" class="text-center">Subtotal</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        <c:set var="total" value="${0.0}"/>
        <c:forEach var="cart" items="${cartList}">
            <tr>
                <td data-th="Product">
                    <div class="row">
                        <div class="col-sm-2 hidden-xs"><img
                                src="<c:out value="${cart.product.book.imageUrl}"> </c:out>" alt="..."
                                class="img-responsive"/></div>
                        <div class="col-sm-10">
                            <h4 class="nomargin"><a href="<c:url value="/product/${cart.product.id}"/>">
                                <c:out value="${cart.product.book.title}"></c:out></a></h4>

                            <p><c:out value="${cart.product.book.language}"></c:out></p>
                        </div>
                    </div>
                </td>
                <td data-th="Price">TK. <c:out value="${cart.product.basePrice}"></c:out><br>
                    <c:out value="(${cart.product.discount}% Discount)"></c:out></td>
                <td data-th="Quantity">
                    <c:out value="${cart.quantity}"></c:out>
                </td>
                <td data-th="Subtotal" class="text-center">TK. <c:out value="${cart.subTotalPrice}"></c:out></td>
                <c:set var="total" value="${total + cart.subTotalPrice}"/>
            </tr>
        </c:forEach>
        <tr>
            <td data-th="Product"></td>
            <td data-th="Price"></td>
            <td data-th="Quantity"></td>
            <td data-th="Subtotal" class="text-center"><strong>Shipping Cost: TK. <c:out
                    value="${shippingCost}"> </c:out></strong></td>
        </tr>
        <tr>
            <td data-th="Product"></td>
            <td data-th="Price"></td>
            <td data-th="Quantity"></td>
            <td data-th="Subtotal" class="text-center"><strong>Total Payable TK.
                <c:out value="${total + shippingCost}"></c:out></strong></td>
        </tr>
        </tbody>
    </table>

    <h4> Shipping address:</h4>
    <c:out value="${user.firstName} ${user.lastName}"> </c:out> <br>
    <c:out value="${user.phone}"> </c:out> <br>
    <c:out value="${shippingAddress}"> </c:out> <br> <br>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#cash"><h4>CASH ON DELIVERY</h4></a></li>
        <li><a data-toggle="tab" href="#bkash"><h4>MOBILE PAYMENT</h4></a></li>
    </ul>

    <div class="tab-content">
        <div id="cash" class="tab-pane fade in active">

            <p class="header"><h4>How to Pay</h4></p>
            <ul>
                <li>Click on "Confirm Order".</li>
                <li>Your order will be placed immediately.</li>
                <li>You will get the parcel of happiness within 3-5 working days(in Dhaka).</li>
                <li>After receiving the parcel, pay to the delivery man.</li>
            </ul>
            <form method="post" action="/foodbank/cart/order/save">
                <input type="hidden" name="paymentOption" value="cashOnDelivery">
                <input type="submit" value="Confirm Order" class="btn btn-success"/>
            </form>
        </div>
        <div id="bkash" class="tab-pane fade">
            <p class="header"> <h4>How to Pay</h4></p>
            <ul>
                <li>Go to your bKash Mobile Menu by dialing *247#</li>
                <li>Select 3 for Payment</li>
                <li>Enter <strong>01841113003</strong> as the Merchant bKash Account Number</li>
                <li>Enter the amount you want to pay</li>
                <li>Enter a reference* against your payment (i.e. your order id)</li>
                <li>Enter the Counter Number 1</li>
                <li>Now enter your PIN to confirm</li>
                <li>After entering PIN, you will get the confirmation SMS.</li>
                <li>From the confirmation SMS, <strong>Enter the Transaction Id below.</strong></li>
            </ul>
            <form method="post" action="/foodbank/cart/order/save">
                <h4>Transaction Id:</h4><input type="text" name="transactionId">
                <input type="hidden" name="paymentOption" value="bkash">
                <input type="submit" value="Confirm Order" class="btn btn-success"/>
            </form>
        </div>
    </div>
</div>
</body>
</html>