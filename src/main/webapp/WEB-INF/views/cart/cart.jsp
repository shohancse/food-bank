<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Foodbank</title>

    <link href="<c:url value="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" />"
          rel="stylesheet">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <style type="text/css">
        .table > tbody > tr > td, .table > tfoot > tr > td {
            vertical-align: middle;
        }

        @media screen and (max-width: 600px) {
            table#cart tbody td .form-control {
                width: 20%;
                display: inline !important;
            }

            .actions .btn {
                width: 36%;
                margin: 1.5em 0;
            }

            .actions .btn-info {
                float: left;
            }

            .actions .btn-danger {
                float: right;
            }

            table#cart thead {
                display: none;
            }

            table#cart tbody td {
                display: block;
                padding: .6rem;
                min-width: 320px;
            }

            table#cart tbody tr td:first-child {
                background: #333;
                color: #fff;
            }

            table#cart tbody td:before {
                content: attr(data-th);
                font-weight: bold;
                display: inline-block;
                width: 8rem;
            }

            table#cart tfoot td {
                display: block;
            }

            table#cart tfoot td .btn {
                display: block;
            }
        }
    </style>
</head>
<body>

<div class="container">
    <h3>My Cart</h3>
    <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Product</th>
            <th style="width:10%">Price</th>
            <th style="width:8%">Quantity</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="cart" items="${cartList}">
            <tr>
                <td data-th="Product">
                    <div class="row">
                        <div class="col-sm-2 hidden-xs"><img
                                src="<c:out value="${cart.product.book.imageUrl}"> </c:out>" alt="..."
                                class="img-responsive"/></div>
                        <div class="col-sm-10">
                            <h4 class="nomargin"><a href="<c:url value="/product/${cart.product.id}"/>">
                                <c:out value="${cart.product.book.title}"></c:out></a></h4>

                            <p><c:out value="${cart.product.book.language}"></c:out></p>
                        </div>
                    </div>
                </td>
                <td data-th="Price">TK. <c:out value="${cart.product.basePrice}"></c:out><br>
                    <c:out value="(${cart.product.discount}% Discount)"></c:out></td>
                <td data-th="Quantity">
                    <form method="post" action="edit">
                        <input type="number" class="form-control text-center" name="quantity" min="1" max="10"
                               value="${cart.quantity}">
                        <input type="hidden" name="cartId" value="${cart.id}">
                        <input type="submit" value="Update" class="btn btn-info btn-block"/>
                    </form>
                </td>
                <td>
                    <form method="post" action="remove">
                        <input type="hidden" name="cartId" value="${cart.id}">
                        <input type="submit" value="Remove" class="btn btn-danger"/>
                    </form>
                </td>
            </tr>
        </c:forEach>
        </tbody>
        <tr>
            <td><a href="<c:url value="/"/>" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a>
            </td>
            <td colspan="2" class="hidden-xs"></td>
            <td>
                <form method="get" action="shipping">
                    <input type="submit" value="Checkout" class="btn btn-success btn-block"/>
                </form>
            </td>
        </tr>
    </table>
</div>

<script src="<c:url value="/resources/lib/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/lib/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>