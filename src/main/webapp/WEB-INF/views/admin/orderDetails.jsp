<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class="container">

    <div class="row">

        <jsp:include page="navigation.jsp"/>

        <%--Show Particular Order--%>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default shadow-depth-1">

                <div class="panel-heading">
                    <spring:message code="label.orderId"/>${order.id}
                    (${order.orderStatus})
                </div>

                <div class="col-xs-12">
                    <br>

                    <div class="row">
                        <div class="col-xs-12">
                            <c:choose>
                                <c:when test="${order.orderStatus == 'PENDING'}">
                                    <form:form action="${order.id}/process" method="post">
                                        <input name="orderStatus" value="PROCESSING" type="hidden">
                                        <button type="submit" class="btn btn-success">
                                            <spring:message code="label.process"/>
                                        </button>
                                    </form:form>
                                    <br>
                                    <form:form action="${order.id}/discard" method="post">
                                        <input name="orderStatus" value="DISCARDED" type="hidden">
                                        <button type="submit" class="btn btn-danger">
                                            <spring:message code="label.discard"/>
                                        </button>
                                    </form:form>
                                </c:when>
                                <c:when test="${order.orderStatus == 'PROCESSING'}">
                                    <form:form action="${order.id}/deliver" method="post">
                                        <spring:message code="label.deliveryDate"/>
                                        <input name="date" type="date" placeholder="Delivery Time" required="required">
                                        <input name="orderStatus" value="DELIVERED" type="hidden">
                                        <button type="submit" class="btn btn-success">
                                            <spring:message code="button.deliver"/>
                                        </button>
                                    </form:form>
                                </c:when>
                                <c:when test="${order.orderStatus == 'DISCARDED'}">
                                    <div class="alert-danger">
                                        <spring:message code="label.discarded"/>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="alert-success">
                                        <spring:message code="label.delivered"/><br>
                                        <spring:message code="label.deliveryDate"/>
                                        <fmt:formatDate value="${order.deliveryDate}" type="both" dateStyle="long"
                                                        timeStyle="long"/>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-6">
                            <address>
                                <strong>Customer:</strong><br>
                                <spring:message code="label.name"/> : ${order.user.firstName} ${order.user.lastName}<br>
                                <spring:message code="label.address"/> : ${order.destination}<br>
                                <spring:message code="label.phone"/> : ${order.user.phone}
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <address>
                                <strong><spring:message code="label.paymentMethod"/></strong><br>
                                <c:choose>
                                    <c:when test="${order.paymentOption == 'BKASH'}">
                                        <spring:message code="label.bkash"/><br>
                                        <spring:message code="label.transactionID"/>${order.bkashTransactionId}
                                    </c:when>
                                    <c:otherwise>
                                        <spring:message code="label.cashOnDelivery"/>
                                    </c:otherwise>
                                </c:choose>
                            </address>
                        </div>
                        <div class="col-xs-6 text-right">
                            <address>
                                <strong><spring:message code="label.orderDate"/></strong><br>
                                <fmt:formatDate value="${order.checkoutTime}" type="both" dateStyle="long"
                                                timeStyle="long"/><br><br>
                            </address>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><spring:message code="label.product(Quantity)"/></th>
                            <c:if test="${order.orderStatus=='PENDING'}">
                                <th><spring:message code="label.stockQuantity"/></th>
                            </c:if>
                            <th><spring:message code="label.fromStore"/></th>
                            <th><spring:message code="label.price"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${order.orderedProductList}" var="orderedProduct">
                            <tr>
                                <td>${orderedProduct.product.book.title} (${orderedProduct.quantity})</td>

                                <c:if test="${order.orderStatus=='PENDING'}">
                                    <c:choose>
                                        <c:when test="${orderedProduct.product.stockQuantity >= orderedProduct.quantity }">
                                            <td>${orderedProduct.product.stockQuantity}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td>
                                                <div class="alert-danger">
                                                        ${orderedProduct.product.stockQuantity}
                                                    <spring:message code="label.outOfStock"/>
                                                </div>
                                            </td>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>

                                <td>${orderedProduct.product.store.name}</td>
                                <td>${orderedProduct.subTotalPrice}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-11 text-right">
                        <address>
                            <spring:message code="label.shippingCost"/>${order.shippingCost}<br>
                            <strong><spring:message code="label.totalCost"/>${order.totalPrice}</strong>
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>