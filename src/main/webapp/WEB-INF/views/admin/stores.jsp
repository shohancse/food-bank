<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class="container">

    <div class="row">

        <jsp:include page="navigation.jsp"/>

        <%--Show Stores--%>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading"><spring:message code="label.stores"/></div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><spring:message code="label.storeName"/></th>
                            <th><spring:message code="label.status"/></th>
                            <th><spring:message code="label.storeKeeperName"/></th>
                            <th><spring:message code="button.viewDetails"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${storeList}" var="store">
                            <tr>
                                <td>${store.name}</td>
                                <td>${store.status}</td>
                                <td>${store.user.firstName} ${store.user.lastName}</td>
                                <td>
                                    <a class="btn btn-info"
                                       href="${pageContext.servletContext.contextPath}/admin/store/${store.id}">
                                        <spring:message code="label.viewDetails"/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
</body>