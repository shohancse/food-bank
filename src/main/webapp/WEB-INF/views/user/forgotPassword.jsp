<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Foodbank :: Verification</title>

    <link href="<c:url value="/resources/css/user-style.css" />" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/css/shadow.css" />" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container">

    <br>
    <br>
    <br>

    <div class="row">
        <div class="col-md-12 col-sm-10">
            <div class="shadow-depth-1">
                <div class="row text-center">
                    <div class="col-sm-6 col-sm-offset-3">


                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

</body>
</body>
</html>
