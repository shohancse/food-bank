<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Foodbank :: Orders</title>

    <link href="<c:url value="/resources/css/user-style.css" />" rel="stylesheet"/>
</head>
<body>
<div class="container">

    <br>
    <br>
    <br>

    <div class="row">

        <%@include file="../jspf/userNavPill.jspf" %>

        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading">
                    <h4><spring:message code="pill.purchases"/>&nbsp;</h4>
                </div>
                <div class="panel-body">

                    <div class="row">

                        <c:forEach items="${productList}" var="product">

                            <!-- BEGIN PRODUCTS -->
                            <div class="col-md-3 col-sm-3">
    		                <span class="thumbnail">
                                <div class="row">
                                    <img class="col-md-8"
                                         src="https://s3-ap-southeast-1.amazonaws.com/rokomari110/product/imgrok0610_8022.GIF"
                                         alt="book">
                                </div>

                                <div class="row product-desc">
                                    <div class="col-md-10">
                                        <h4 style="text-align: center"><c:out value="${product.bookTitle}"/></h4>
                                        <p style="text-align: center"><c:out value="${product.author}"/></p>

                                        <div class="ratings">
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star-empty"></span>
                                        </div>
                                        <hr class="line">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <p class="price">TK <c:out value="${product.price}"/> </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    		                 </span>
                            </div>

                        </c:forEach>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
