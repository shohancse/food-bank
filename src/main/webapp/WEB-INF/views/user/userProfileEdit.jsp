<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Foodbank :: Profile</title>
</head>
<body>
<div class="container">

    <br>
    <br>
    <br>

    <div class="row">

        <%@include file="../jspf/userNavPill.jspf" %>

        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading">
                    <h4>Profile&nbsp;</h4>
                </div>
                <div class="panel-body">
                    <c:url var="postUrl" value="/user/profile-edit"/>
                    <form:form commandName="userProfileForm" class="form-horizontal" method="post" action="${postUrl}">
                        <fieldset>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="firstname"><spring:message
                                        code="label.name"/></label>

                                <div class="col-md-3">
                                    <form:input path="firstName" id="firstname" name="firstname" type="text"
                                                placeholder="your first name"
                                                class="form-control input-md" required="required"/>

                                    <form:errors path="firstName" class="text-danger"/>

                                </div>
                                <div class="col-md-3">
                                    <form:input path="lastName" id="lastname" name="lastName" type="text"
                                                placeholder="your last name"
                                                class="form-control input-md" required="required"/>

                                    <form:errors path="lastName" class="text-danger"/>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="email">
                                    <spring:message code="label.email"/></label>

                                <div class="col-md-6">
                                    <form:input path="email" id="email" name="email" type="text"
                                                placeholder="your email address"
                                                class="form-control input-md" disabled="true" required="required"/>

                                    <form:errors path="email" class="text-danger"/>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="phone">
                                    <spring:message code="label.phone"/></label>

                                <div class="col-md-6">
                                    <form:input path="phone" id="phone" name="phone" type="text"
                                                placeholder="eg. 017XX XXXXXX"
                                                class="form-control input-md" required="required"/>

                                    <form:errors path="phone" class="text-danger"/>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="address"><spring:message
                                        code="label.address"/></label>

                                <div class="col-md-6">
                                    <form:input path="address" id="address" name="address" type="text"
                                                placeholder="your address"
                                                class="form-control input-md" required="required"/>

                                    <form:errors path="address" class="text-danger"/>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="dob"><spring:message
                                        code="label.dob"/></label>

                                <div class="col-md-6">
                                    <form:input path="dateOfBirth" id="dob" name="dateOfBirth" type="date"
                                                class="form-control input-md"
                                                required="required"/>

                                    <form:errors path="dateOfBirth" class="text-danger"/>
                                </div>
                            </div>

                            <!-- Multiple Radios (inline) -->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="gender-m"><spring:message
                                        code="label.gender"/></label>

                                <div class="col-md-6">
                                    <label class="radio-inline" for="gender-m">
                                        <form:radiobutton path="gender" name="gender" id="gender-m" value="MALE"/>
                                        <spring:message code="label.gender.male"/>
                                    </label>
                                    <label class="radio-inline" for="gender-f">
                                        <form:radiobutton path="gender" name="gender" id="gender-f" value="FEMALE"/>
                                        <spring:message code="label.gender.female"/>
                                    </label>

                                    <form:errors path="gender" class="text-danger"/>
                                </div>
                            </div>

                            <!-- Textarea -->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="bio">
                                    <spring:message code="label.bio"/></label>

                                <div class="col-md-6">
                                    <form:textarea path="bio" class="form-control" id="bio" name="bio"
                                                   placeholder="Write about yourself"/>

                                    <form:errors path="bio" class="text-danger"/>
                                </div>
                            </div>

                            <!-- File Button -->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="photoButton">
                                    <spring:message code="label.photo"/>
                                </label>

                                <div class="col-md-6">
                                    <input id="photoButton" name="photo"
                                           class="btn btn-default input-file"
                                           type="file"/>
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="submit"></label>

                                <div class="col-md-8">
                                    <button id="submit" type="submit" class="btn btn-success">
                                        <spring:message code="button.submit"/>
                                    </button>
                                </div>
                            </div>

                        </fieldset>
                    </form:form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
