<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Foodbank :: Verification</title>

    <link href="<c:url value="/resources/css/user-style.css" />" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/css/shadow.css" />" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container">

    <br>
    <br>
    <br>

    <div class="row">
        <div class="col-md-12 col-sm-10">
            <div class="shadow-depth-1">
                <div class="row text-center">
                    <div class="col-sm-6 col-sm-offset-3">
                        <br><br>

                        <h2 style="color:#0fad00"><spring:message code="label.verificationSuccess"/></h2>
                        <img src="<c:url value="/resources/images/check-true.jpg"/>">

                        <h3><spring:message code="label.greeting"/>, <c:out value="${user}"/></h3>

                        <p style="font-size:20px;color:#5C5C5C;"><spring:message code="label.verificationMessage"/></p>
                        <a href="<c:url value="/user/login"/>" class="btn btn-success"><spring:message
                                code="button.login"/></a>
                        <br><br>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

</body>
</body>
</html>
