<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Foodbank :: Login</title>

    <link href="<c:url value="/resources/css/user-style.css" />" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container">

<br>
<br>
<br>

<div class="row">
<div class="col-md-6 col-sm-6">
    <div class="login-area shadow-depth-1">

        <form:form commandName="loginForm" class="form-horizontal" action="login" method="post">

            <fieldset>
                <!-- Form Name -->
                <legend><spring:message code="legend.signIn"/></legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="email">
                        <spring:message code="label.email"/>
                    </label>

                    <div class="col-md-8">
                        <form:input path="email" id="email" name="email" type="text" placeholder="your email"
                                    class="form-control input-md" required="required"/>
                        <form:errors path="email" class="text-danger"/>
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="password">
                        <spring:message code="label.password"/>
                    </label>

                    <div class="col-md-8">
                        <form:input path="password" id="password" name="password" type="password"
                                    placeholder="placeholder"
                                    class="form-control input-md" required="required"/>
                        <form:errors path="password" class="text-danger"/>
                    </div>
                </div>


                <!-- Checkbox input-->
                <div class="form-group">
                    <label class="col-md-4 control-label"></label>

                    <div class="row checkbox col-md-8">
                        <label>
                            <form:checkbox path="rememberMe" value="false"/><spring:message
                                code="label.rememberMe"/>
                        </label>


                        <label><a href="<c:url value="/user/forgot-password"/>">
                            <spring:message code="label.forgotPass"/>
                        </a></label>
                    </div>
                </div>


                <!-- Button -->
                <div class=" form-group">
                    <label class="col-md-4 control-label" for="login"></label>

                    <div class="col-md-8">
                        <button id="login" name="" class="btn btn-info">
                            <spring:message code="button.login"/>
                        </button>
                    </div>
                </div>


                <!-- Alert -->
                <c:if test="${loginFail != null}">
                    <div class="alert alert-danger alert-dismissable fade in shadow-depth-2">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><spring:message code="login.failTitle"/></strong><spring:message
                            code="login.failMessage"/>
                    </div>
                </c:if>

            </fieldset>
        </form:form>
    </div>
</div>
<div class="col-md-6 col-sm-6">
    <div class="signup-area shadow-depth-1">

        <form:form commandName="signUpForm" class="form-horizontal" action="signup" method="post">
            <fieldset>

                <!-- Form Name -->
                <legend><spring:message code="legend.signUp"/></legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="firstname">
                        <spring:message code="label.name"/>
                    </label>

                    <div class="col-md-4">
                        <form:input path="firstName" id="firstname" name="firstname" type="text"
                                    placeholder="your first name"
                                    class="form-control input-md" required="required"/>
                        <form:errors path="firstName" class="text-danger"/>

                    </div>
                    <div class="col-md-4">
                        <form:input path="lastName" id="lastname" name="lastname" type="text"
                                    placeholder="your last name"
                                    class="form-control input-md" required="required"/>
                        <form:errors path="lastName" class="text-danger"/>
                    </div>
                </div>


                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="signupemail">
                        <spring:message code="label.email"/>
                    </label>

                    <div class="col-md-8">
                        <form:input path="email" id="signupemail" name="email" type="text"
                                    placeholder="your email address"
                                    class="form-control input-md" required="required"/>
                        <form:errors path="email" class="text-danger"/>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="phone">
                        <spring:message code="label.phone"/>
                    </label>

                    <div class="col-md-8">
                        <form:input path="phone" id="phone" name="phone" type="text"
                                    placeholder="eg. 017XX XXXXXX"
                                    class="form-control input-md" required="required"/>
                        <form:errors path="phone" class="text-danger"/>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="address">
                        <spring:message code="label.address"/>
                    </label>

                    <div class="col-md-8">
                        <form:input path="address" id="address" name="address" type="text"
                                    placeholder="your address"
                                    class="form-control input-md" required="required"/>
                        <form:errors path="address" class="text-danger"/>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="dob">
                        <spring:message code="label.dob"/>
                    </label>

                    <div class="col-md-8">
                        <form:input path="dateOfBirth" id="dob" name="dateOfBirth" type="date"
                                    pattern="yyyy-MM-dd"
                                    class="form-control input-md"
                                    required="required"/>
                        <form:errors path="dateOfBirth" class="text-danger"/>
                    </div>
                </div>

                <!-- Multiple Radios (inline) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="gender-m">
                        <spring:message code="label.gender"/>
                    </label>

                    <div class="col-md-8">
                        <label class="radio-inline" for="gender-m">
                            <form:radiobutton path="gender" name="gender" id="gender-m" value="MALE"/>
                            <spring:message
                                    code="label.gender.male"/>
                        </label>
                        <label class="radio-inline" for="gender-f">
                            <form:radiobutton path="gender" name="gender" id="gender-f" value="FEMALE"/>
                            <spring:message
                                    code="label.gender.female"/>
                        </label>

                        <form:errors path="gender" class="text-danger"/>
                    </div>
                </div>


                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="signup-pass">
                        <spring:message code="label.password"/>
                    </label>

                    <div class="col-md-8">
                        <form:input path="password" id="signup-pass" name="signup-pass" type="password"
                                    placeholder=""
                                    class="form-control input-md" required="required"/>
                        <form:errors path="password" class="text-danger"/>
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="verifyPassword">
                        <spring:message code="label.verifyPassword"/></label>

                    <div class="col-md-8">
                        <form:input path="verifyPassword" id="verifyPassword" name="verifyPassword"
                                    type="password"
                                    class="form-control input-md" required="required"/>
                        <form:errors path="verifyPassword" class="text-danger"/>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="signup"></label>

                    <div class="col-md-8">
                        <button id="signup" type="submit" class="btn btn-success">
                            <spring:message code="button.submit"/>
                        </button>
                    </div>
                </div>


                <!-- Alert -->
                <c:if test="${signUpSuccess != null}">
                    <div class="alert alert-success alert-dismissable fade in shadow-depth-2">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><spring:message code="signup.successTitle"/></strong>
                        <spring:message code="signup.successMessage"/>
                    </div>
                </c:if>

            </fieldset>
        </form:form>


    </div>

</div>

</div>

</div>

</body>
</body>
</html>
