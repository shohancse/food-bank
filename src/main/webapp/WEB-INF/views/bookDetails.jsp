<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Details</title>

    <link href="<c:url value="/resources/css/home-style.css" />" rel="stylesheet" type="text/css"/>

    <style type="text/css">
        div.stars {
            width: 270px;
            display: inline-block;
        }

        input.star {
            display: none;
        }

        label.star {
            float: right;
            padding: 10px;
            font-size: 36px;
            color: #444;
            transition: all .2s;
        }

        input.star:checked ~ label.star:before {
            content: '\f005';
            color: #FD4;
            transition: all .25s;
        }

        input.star-5:checked ~ label.star:before {
            color: #FE7;
            text-shadow: 0 0 20px #952;
        }

        input.star-1:checked ~ label.star:before {
            color: #F62;
        }

        label.star:hover {
            transform: rotate(-15deg) scale(1.3);
        }

        label.star:before {
            content: '\f006';
            font-family: FontAwesome;
        }
    </style>
</head>
<body>

    <c:set var="productId" scope="session" value="${singleProduct.product.id}"/>


<div class="container">
    <br>
    <br>
    <br>

    <div class="row">

        <div class="col-sm-4 col-md-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="card">
                        <img class="img-thumbnail" src="<c:url value ="/resources/images/categoryicon.png" />"
                             alt="thumb01" width="304" height="236">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-md-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div>
                        <h3><c:out value="${singleProduct.product.book.title}"></c:out></h3>

                        <p>Author: <c:out value="${singleProduct.authors}"></c:out></p>

                        <p>Publisher: <c:out value="${singleProduct.product.book.publisher.name}"></c:out></p>

                        <p>Store: <c:out value="${singleProduct.product.store.name}"></c:out></p>
                    </div>
                    <div>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star-empty"></span>
                        <span class="glyphicon glyphicon-star-empty"></span>
                        <span>2 ratings</span>
                    </div>
                    <br>

                    <div class="well">
                        <span>Current Price:</span>&nbsp;&nbsp;&nbsp;<span>${singleProduct.currentPrice}</span>
                        <br>
                        <span>Discount:</span>&nbsp;&nbsp;&nbsp;<span style="color: #ff0000">${singleProduct.product.discount}%</span>
                        <br>
                        <c:set var="strike" value="none"></c:set>
                        <c:if test="${singleProduct.product.basePrice ne 0.0}">
                            <c:set var="strike" value="line-through"></c:set>
                        </c:if>

                        <span style="text-decoration: ${strike}">Regular Price:</span>&nbsp;&nbsp;&nbsp;<span style="text-decoration: ${strike}">${singleProduct.product.basePrice}</span>
                        <br>
                        <span>Shipping:</span>&nbsp;&nbsp;<span>Tk.30</span>
                    </div>
                    <div>
                        <form method="post" action="/foodbank/cart/add">
                            <input type="hidden" name="productId" value="${singleProduct.product.id}">
                            <input type="submit" value="Add to Cart" class="btn btn-primary"/>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="container">
        <div class="row">
<%--            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p><b><spring:message
                                code="product.rateThisProduct"/></b></p>

                        <div class="stars">
                            <form action="/foodbank/product/rating" method="get">
                                <input class="star star-5" id="star-5" type="checkbox" value="5" name="rating"/>
                                <label class="star star-5" for="star-5"></label>

                                <input class="star star-4" id="star-4" type="checkbox" value="4" name="rating"/>
                                <label class="star star-4" for="star-4"></label>

                                <input class="star star-3" id="star-3" type="checkbox" value="3" name="rating"/>
                                <label class="star star-3" for="star-3"></label>

                                <input class="star star-2" id="star-2" type="checkbox" value="2" name="rating"/>
                                <label class="star star-2" for="star-2"></label>

                                <input class="star star-1" id="star-1" type="checkbox" value="1" name="rating"/>
                                <label class="star star-1" for="star-1"></label>

                                <input type="hidden" name="productId" value="${singleProduct.product.id}">
                                <input type="submit" value="Rate" class="btn btn-success"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>--%>
        </div>

        <div id="simCatBooks" class="tab-pane fade in active">
            <h3><spring:message code="product.otherBooks"/></h3>
        </div>

        <br>

        <c:if test="${singleProduct.listOfProductsFromSimilarCategory.size()>1}">
            <div id="simCatBooks" class="tab-pane fade in active">
                <h3><c:out value="${singleProduct.category.name}"></c:out></h3>
            </div>

            <div class="row">
                <c:forEach var="product" items="${singleProduct.listOfProductsFromSimilarCategory}">
                    <c:if test="${product.id != productId}">
                        <div class="col-sm-6 col-md-3">

                            <div class="card">
                                <img src="<c:url value ="/resources/images/categoryicon.png" />">

                                <div class="well"><a href="<c:url value="/product/${product.id}"/>">${product.book.title}</a></div>
                            </div>
                        </div>
                    </c:if>
                </c:forEach>

            </div>
        </c:if>

        <c:if test="${singleProduct.listOfProductsFromSimilarPublisher.size()>1}">
            <div id="simPubBooks" class="tab-pane fade in active">
                <h3><c:out value="${singleProduct.product.book.publisher.name}"></c:out></h3>
            </div>

            <div class="row">

                <c:forEach var="product" items="${singleProduct.listOfProductsFromSimilarPublisher}">
                    <c:if test="${product.id != productId}">
                        <div class="col-sm-6 col-md-3">

                            <div class="card">
                                <img src="<c:url value ="/resources/images/categoryicon.png" />">

                                <div class="well"><a href="<c:url value="/product/${product.id}"/>">${product.book.title}</a></div>
                            </div>
                        </div>
                    </c:if>
                </c:forEach>

            </div>
        </c:if>
        
        <c:if test="${singleProduct.listOfProductsFromSimilarAuthor.size()>1}">
            <div id="simAuthBooks" class="tab-pane fade in active">
                <h3><c:out value="${singleProduct.authors}"></c:out></h3>
            </div>

            <div class="row">

                <c:forEach var="product" items="${singleProduct.listOfProductsFromSimilarAuthor}">
                    <c:if test="${product.id != productId}">
                        <div class="col-sm-6 col-md-3">

                            <div class="card">
                                <img src="<c:url value ="/resources/images/categoryicon.png" />">

                                <div class="well"><a href="<c:url value="/product/${product.id}"/>">${product.book.title}</a></div>
                            </div>
                        </div>
                    </c:if>
                </c:forEach>

            </div>
        </c:if>

        <c:if test="${singleProduct.listOfProductsFromSimilarStore.size()>1}">
            <div id="simStoreBooks" class="tab-pane fade in active">
                <h3><c:out value="${singleProduct.product.store.name}"></c:out></h3>
            </div>

            <div class="row">

                <c:forEach var="product" items="${singleProduct.listOfProductsFromSimilarStore}">
                    <c:if test="${product.id != productId}">
                        <div class="col-sm-6 col-md-3">

                            <div class="card">
                                <img src="<c:url value ="/resources/images/categoryicon.png" />">

                                <div class="well"><a href="<c:url value="/product/${product.id}"/>">${product.book.title}</a></div>
                            </div>
                        </div>
                    </c:if>
                </c:forEach>

            </div>
        </c:if>

    </div>

    <div class="container">
        <hr>
        <footer>
            <div class="row">
                <div class="col-md-12">
                    <p class="pull-right"><a href="#">Back to top</a></p>
                    <p> Copyright &copy; 2016. Foodbank</p>
                </div>
            </div>
        </footer>
    </div>
    <script src="<c:url value="/resources/lib/jquery-3.1.1.min.js" />"></script>
    <script src="<c:url value="/resources/lib/bootstrap/js/bootstrap.min.js" />"></script>
</div>
</body>
</html>