<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html lang="en">
<head>
    <title>Foodbank</title>

    <link href="<c:url value="/resources/css/home-style.css" />" rel="stylesheet" type="text/css"/>
</head>

<body>

<hr>

<!-- Main Navigation Bar Start -->
<nav class="navbar-inverse">
    <div>
        <ul class="nav nav-justified">
            <li class="active"><a href="<c:url value="/categories"/>">Categories</a></li>
            <li><a href="<c:url value="/authors"/>">Authors</a></li>
            <li><a href="<c:url value="/publishers"/>">Publishers</a></li>
            <li><a href="<c:url value="/stores"/>">Stores</a></li>
        </ul>
    </div>
</nav>
<!-- Main Navigation Bar End -->
<hr>
<!-- Banner Image Start -->
<img class="img-responsive center-block" src="<c:url value ="/resources/images/banner.jpg" />">
<!-- Banner Image End -->

<hr>
<div class="row">
    <ul class="list-inline list-unstyled">
        <div class="col-sm-12 col-md-12 text-center">
            <li>
                <h3>Search Your Favourite <c:out value="${viewType}"></c:out></h3>
            </li>
            <li>
                <form class="navbar-form" role="search" action="/foodbank/${viewUrl}/search">
                    <div class="input-group">
                        <input type="text" name="searchKeyWord" class="form-control" placeholder="Enter Your Search Words">

                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </li>
        </div>
    </ul>
</div>


<hr>
<div class="container">
    <div class="row marketing">
        <c:forEach var="viewItem" items="${viewList}">
            <a href="<c:url value="/${viewUrl}/${viewItem.id}"/>">
                <div class="col-sm-6 col-md-4">
                    <div class="card">
                        <img src=
                            <c:url value="/resources/images/${viewIcon}"></c:url>>

                        <div class="well">${viewItem.name}</div>
                    </div>
                </div>
            </a>
        </c:forEach>
    </div>
</div>

<!-- Pagination Starts -->
<div class="text-center">
    <ul class="pagination">
        <%--For displaying Previous link except for the 1st page --%>
        <c:if test="${currentPage != 1}">
            <li><a href="<c:url value="/${viewUrl}?page=${currentPage - 1}"/>"><< previous</a></li>
        </c:if>
        <%--For displaying Page numbers.
        The when condition does not display a link for the current page--%>
        <c:forEach begin="1" end="${noOfPages}" var="i">
            <c:choose>
                <c:when test="${currentPage eq i}">
                    <li class="active"><a>${i}</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="/<c:url value="${viewUrl}?page=${i}"/>">${i}</a></li>
                </c:otherwise>
            </c:choose>
        </c:forEach>

        <%--For displaying Next link --%>
        <c:if test="${currentPage lt noOfPages}">
            <li><a href="/<c:url value="${viewUrl}?page=${currentPage + 1}"/>">next >></a></li>
        </c:if>
    </ul>
</div>
<!-- Pagination Ends -->
<hr>
<!-- Footer Start -->
<div>
    <footer>
        <p class="pull-right"><a href="#">Back To Top</a></p>

        <p>Designed By Company . <a href="#">Privacy</a> . <a href="#">Terms</a></p>
    </footer>
</div>
</div>
<!-- Footer End -->
</body>
</html>
