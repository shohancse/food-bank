<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><c:out value="Add Product"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="navigation.jsp"/>
<div class="col-md-9 col-sm-9">
    <div class="panel panel-default shadow-depth-1">
        <div class="panel-heading"><c:out value="Add New Product"/></div>
        <div class="panel-body">
            <div class="row">
                <jsp:include page="logMessage.jsp"/>
                <springForm:form action="add/book" method="GET">
                    <select class="form-control" name="selectedBook" required>
                        <option value= <c:out value=""/>><c:out value="Select the Book"/></option>
                        <c:forEach items="${bookList}" var="book">
                            <option value="<c:out value="${book.id}"/>">
                                <c:out value="${book.title} (publisher: ${book.publisher.name})"/>
                            </option>
                        </c:forEach>
                        <option value= <c:out value="otherBook"/>><c:out value="Not in the List, Create New."/>
                    </select>
                    <br><br>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default"><c:out value="Submit"/></button>
                        </div>
                    </div>
                </springForm:form>
            </div>
        </div>
    </div>
</div>


</body>
</html>
