<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class="container">

    <div class="row">

        <jsp:include page="navigation.jsp"/>

        <div class="col-md-9 col-sm-9">

            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading"><spring:message text="Customer Orders"/></div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><spring:message text="Product Id"/></th>
                            <th><spring:message text="Product Name"/></th>
                            <th><spring:message text="Quantity"/></th>
                            <th><spring:message text="Sub Total"/></th>
                            <th><spring:message text="Order Status"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${orderList}" var="order">
                            <tr>
                                <td><c:out value="${order.product.id}"/></td>
                                <td><c:out value="${order.product.book.title}"/></td>
                                <td><c:out value="${order.quantity}"/></td>
                                <td><c:out value="${order.subTotalPrice}"/></td>
                                <td><c:out value="${order.order.orderStatus}"/></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>

                    <c:if test="${page >'0'}">
                        <a class="btn btn-default"
                           href="${pageContext.servletContext.contextPath}/mystores/order/${page-1}"><spring:message
                                code="button.previous"/></a>
                    </c:if>

                    <c:if test="${countMyNextOrders >'0'}">
                        <a class="btn btn-default"
                           href="${pageContext.servletContext.contextPath}/mystores/order/${page+1}"><spring:message
                                code="button.next"/></a>
                    </c:if>

                </div>
            </div>
        </div>
    </div>
</div>
</body>

