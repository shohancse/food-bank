<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><c:out value="Add Product"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="navigation.jsp"/>
<div class="col-md-9 col-sm-9">
    <div class="panel panel-default shadow-depth-1">
        <div class="panel-heading"><c:out value="Products"/></div>
        <div class="panel-body">
            <div class="row">
                <jsp:include page="logMessage.jsp"/>
                <c:forEach items="${productList}" var="product">
                    <div class="col-md-3 col-sm-3">
    		                <span class="thumbnail">
                                <div class="row">
                                    <img class="col-md-8"
                                         src="https://s3-ap-southeast-1.amazonaws.com/rokomari110/product/rokimg_20140907_73051.gif"
                                         alt="book">
                                </div>

                                <div class="row product-desc">
                                    <div class="col-md-10">
                                        <h4 style="text-align: center"><c:out value="${product.book.title}"/></h4>

                                        <p style="text-align: center"><c:out value="${product.basePrice} taka"/></p>
                                        <c:choose>
                                            <c:when test="${product.isVerified eq 1}">
                                                <p style="text-align: center; color: green"><b><c:out
                                                        value="Available in the Shop"/></b></p>
                                            </c:when>
                                            <c:otherwise>
                                                <p style="text-align: center; color: orange"><b><c:out
                                                        value="Pending for Approval"/></b></p>
                                            </c:otherwise>
                                        </c:choose>
                                        <c:if test="${isStoreOwner eq 'true'}">
                                            <hr class="line">
                                            <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <p class="btn-add">
                                                    <a class="btn btn-info" href="product/${product.id}/update"
                                                       class="hidden-sm">
                                                        <c:out value="Update Product"/>
                                                    </a>
                                                </p>
                                            </div>
                                        </div>

                                            <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <p class="btn-add">
                                                    <springForm:form method="POST"
                                                                     action="product/${product.id}/delete">
                                                        <button style="background: red; color: white" type="submit"
                                                                class="btn btn-default"><c:out
                                                                value="Delete Product"/></button>
                                                    </springForm:form>
                                                </p>
                                            </div>
                                        </div>
                                        </c:if>

                                    </div>
                                </div>
    		                 </span>
                    </div>
                </c:forEach>
                <c:if test="${isStoreOwner eq 'true'}">
                    <div class="col-md-3 col-sm-3">
    		                <span class="thumbnail">
                                <div class="row">
                                </div>
                                <div class="row product-desc">
                                    <div class="col-md-10">

                                        <h4 style="text-align: center"><c:out value="Want to add a new Product?"/></h4>
                                        <hr class="line">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">

                                                <p class="btn-add">
                                                    <a class="btn btn-info" href="product/add" class="hidden-sm">
                                                        <c:out value="Add New Product"/>
                                                    </a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    		                 </span>
                    </div>
                </c:if>

            </div>


        </div>
    </div>
</div>

</body>
</html>
