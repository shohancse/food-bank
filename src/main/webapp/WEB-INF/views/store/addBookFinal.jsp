<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><c:out value="Details of New Book"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<jsp:include page="navigation.jsp"/>
<div class="col-md-9 col-sm-9">
    <div class="panel panel-default shadow-depth-1">
        <div class="panel-heading"><c:out value="Author and Book Information"/></div>
        <div class="panel-body">
            <div class="row">
                <jsp:include page="logMessage.jsp"/>
                <springForm:form action="productFinal" method="GET">

                    <c:set var="counter" value="1"/>
                    <c:forEach items="${selectedAuthorList}" var="author">
                        <c:choose>
                            <c:when test="${author.name eq null}">
                                <div class="form-group">
                                    <label class="control-label col-sm-2"><c:out value="Author${counter} Name"/></label>

                                    <div class="col-sm-10">
                                        <input type="text" name="name${counter}" class="form-control"
                                               placeholder="${author.name}" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2"><c:out value="Author${counter} Details"/></label>

                                    <div class="col-sm-10">
                                        <input type="text" name="details${counter}" class="form-control"
                                               placeholder="${author.details}" required="">
                                        <input type="hidden" name="imageUrl${counter}" class="form-control"
                                               class="form-control" value="publisher${id}">
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <input type="hidden" name="id${counter}" class="form-control"
                                       placeholder="${author.id}">
                                <input type="hidden" name="name${counter}" class="form-control"
                                       placeholder="${author.name}">
                                <input type="hidden" name="details${counter}" class="form-control"
                                       placeholder="${author.details}">
                                <input type="hidden" name="imageUrl${counter}" class="form-control" class="form-control"
                                       value="${author.imageUrl}">
                            </c:otherwise>
                        </c:choose>
                        <c:set var="counter" value="${counter+1}"/>
                    </c:forEach>

                    <div class="form-group">
                        <label class="control-label col-sm-2"><c:out value="Book Title"/></label>

                        <div class="col-sm-10">
                            <input type="text" name="title" class="form-control" placeholder="" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2"><c:out value="Book Edition"/></label>

                        <div class="col-sm-10">
                            <input type="text" name="edition" class="form-control" placeholder="" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2"><c:out value="Book Country"/></label>

                        <div class="col-sm-10">
                            <input type="text" name="country" class="form-control" placeholder="" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2"><c:out value="ISBN"/></label>

                        <div class="col-sm-10">
                            <input type="text" name="isbn" class="form-control" placeholder="" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2"><c:out value="language"/></label>

                        <div class="col-sm-10">
                            <input type="text" name="language" class="form-control" placeholder="" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2"><c:out value="Number of Pages"/></label>

                        <div class="col-sm-10">
                            <input type="number" name="noOfPages" class="form-control" placeholder="" required=""  min="1">
                        </div>
                        <input type="hidden" name="imageUrl" value="book">
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default"><c:out value="Submit"/></button>
                        </div>
                    </div>
                </springForm:form>
            </div>
        </div>
    </div>
</div>


</body>
</html>