<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><c:out value="Add Product"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<jsp:include page="navigation.jsp"/>

<div class="col-md-9 col-sm-9">
    <div class="panel panel-default shadow-depth-1">
        <div class="panel-heading"><c:out value="My Stores"/></div>
        <div class="panel-body">
            <div class="row">

                <jsp:include page="logMessage.jsp"/>

                <c:forEach items="${storeList}" var="store">
                    <div class="col-md-3 col-sm-3">
    		                <span class="thumbnail">
                                <div class="row">
                                    <img src="<c:url value ="/resources/images/mystore2.jpg" />" alt="img1">
                                </div>
                                <div class="row product-desc">
                                    <div class="col-md-10">
                                        <h4 style="text-align: center"><c:out value="${store.name}"/></h4>
                                        <hr class="line">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <p class="btn-add">
                                                    <c:choose>
                                                        <c:when test="${store.status eq 'APPROVED'}">
                                                                                <a class="btn btn-info"
                                                              href="${pageContext.servletContext.contextPath}/mystores/${store.id}/products"
                                                              class="hidden-sm">
                                                        <c:out value="Open Store"/>
                                                           </a>
                                                        </c:when>
                                                        <c:when test="${store.status eq 'PENDING'}">
                                                            <p style="text-align: center; color: orange"><b><c:out
                                                                    value="Pending"/></b></p>
                                                        </c:when>
                                                <c:otherwise>
                                                    <p style="text-align: center; color: red"><b><c:out
                                                            value="Rejected"/></b></p>
                                                </c:otherwise>

                                                    </c:choose>

                                                    <br><br>
                                                    <c:if test="${store.status ne 'REJECTED'}">
                                                    <a class="btn btn-info"
                                                       href="${pageContext.servletContext.contextPath}/mystores/${store.id}/update"
                                                       class="hidden-sm">
                                                        <c:out value="Update Store"/>
                                                    </a>
                                                    </c:if>
                                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <p class="btn-add">
                                                    <springForm:form method="POST" action="mystores/${store.id}/delete">
                                                        <button style="background: red; color: white" type="submit"
                                                                class="btn btn-default"><c:out
                                                                value="Delete Store"/></button>
                                                    </springForm:form>
                                                </p>
                                            </div>
                                        </div>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    		                 </span>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>

</body>
</html>
