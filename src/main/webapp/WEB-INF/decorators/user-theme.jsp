<?xml version="1.0" encoding="UTF-8" ?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="<c:url value="/resources/lib/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/resources/css/user-style.css" />" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/css/shadow.css" />" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/css/alert.css" />" rel="stylesheet" type="text/css"/>
</head>
<body>

<nav class="navbar navbar-default navbar-default" style="border-radius:0px !important; margin-bottom:0px;">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="container-fluid">

            <div class="navbar-header navbar-middle">
                <a class="navbar-brand navbar-right" href="<c:url value="/"/>">Foodbank</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav"></ul>

                <ul class="nav navbar-nav navbar-right">

                    <c:if test="${USER_ID != null}">
                        <li><a href="<c:url value="/user/logout"/> ">Logout</a></li>
                    </c:if>

                    <c:if test="${USER_ID == null}">
                        <li><a href="<c:url value="/user/login"/> ">Login | Register</a></li>
                    </c:if>

                    <c:if test="${fn:endsWith(pageContext.response.locale,'bn')}">
                        <li><a href="<c:url value="?locale=en" />">English</a></li>
                    </c:if>

                    <c:if test="${fn:endsWith(pageContext.response.locale,'en')}">
                        <li><a href="<c:url value="?locale=bn" />">Bangla</a></li>
                    </c:if>
                </ul>

                <ul class="nav navbar-right">
                    <a class="btn btn-danger navbar-btn" href="<c:url value="/mystores"/>"> Your Store</a>
                </ul>

                <div class="form-group has-feedback">
                    <form class="navbar-form navbar-right">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search Here"/>
                            <i class="glyphicon glyphicon-search form-control-feedback"></i>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</nav>

<hr/>
<decorator:body/>
<hr/>


<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-3">
                <ul class="list-unstyled">
                    <li>GitHub
                    <li>
                    <li><a href="#">About us</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Contact & support</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="list-unstyled">
                    <li>Applications
                    <li>
                    <li><a href="#">Product for Mac</a></li>
                    <li><a href="#">Product for Windows</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="list-unstyled">
                    <li>Services
                    <li>
                    <li><a href="#">Web analytics</a></li>
                    <li><a href="#">Presentations</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="list-unstyled">
                    <li>Documentation
                    <li>
                    <li><a href="#">Product Help</a></li>
                    <li><a href="#">Developer API</a></li>
                </ul>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-8">
                <a href="#">Terms of Service</a>
                <a href="#">Privacy</a>
                <a href="#">Security</a>
            </div>
            <div class="col-md-4">
                <p class="muted pull-right">2016 Foodbank. All rights reserved</p>
            </div>
        </div>
    </div>
</div>
<script src="<c:url value="/resources/lib/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/lib/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
