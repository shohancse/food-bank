<?xml version="1.0" encoding="UTF-8" ?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><spring:message code="label.projectTitle"/></title>
    <link href="<c:url value="/resources/lib/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/resources/css/user-style.css" />" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/css/shadow.css" />" rel="stylesheet" type="text/css"/>
</head>
<body>

<nav class="navbar navbar-default navbar-default" style="border-radius:0px !important; margin-bottom:0px;">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="container-fluid">

            <div class="navbar-header navbar-middle">
                <a class="navbar-brand navbar-right" href="">
                    <spring:message code="label.projectTitle"/>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</nav>

<br>
<decorator:body/>
<hr>

<footer class="footer">
    <div class="container">
        <p class="text-muted">2016 Foodbank. All rights reserved</p>
    </div>
</footer>
<script src="<c:url value="/resources/lib/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/lib/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
