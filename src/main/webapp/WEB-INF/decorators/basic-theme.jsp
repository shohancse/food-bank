<?xml version="1.0" encoding="UTF-8" ?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Foodbank</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="<c:url value="/resources/lib/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/resources/css/font-awsome.css"/>" rel="stylesheet"/>
</head>
<body>

<nav class="navbar navbar-default navbar-default" style="border-radius:0px !important; margin-bottom:0px;">
    <div class="container-fluid">
        <div class="navbar-header navbar-middle">
            <a class="navbar-brand navbar-right" href="<c:url value="/"/> ">Foodbank</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav"></ul>

            <c:set var="searchUrl" value="/foodbank/search"></c:set>
            <form class="navbar-form navbar-right" action=${searchUrl} method="get">
                <div class="form-group">
                    <label for="sel1">Search By</label>
                    <select class="form-control" name="property" id="sel1">
                        <option>Author</option>
                        <option>Publisher</option>
                        <option>Category</option>
                        <option>Store</option>
                    </select>

                </div>
                <div class="input-group">
                    <input type="text" name="searchKeyWord" required = "required" class="form-control" placeholder="Search Tag">

                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>

            <ul class="nav navbar-nav navbar-right">
                <c:if test="${USER_ID != null}">
                    <li><a href="<c:url value="/user/overview"/> ">My Account</a></li>
                </c:if>

                <c:if test="${USER_ID == null}">
                    <li><a href="<c:url value="/user/login"/> ">Login | Register</a></li>
                </c:if>

            </ul>

            <ul class="nav navbar-right">
                <form action="/foodbank/cart/view">
                    <input type="submit" value="View Cart" class="btn btn-primary"/>
                </form>
            </ul>
            <ul class="nav navbar-right">
                <form action="/foodbank/mystores">
                    <input type="submit" value="Your Store" class="btn btn-danger"/>
                </form>
            </ul>
        </div>
    </div>
</nav>

<hr/>
<decorator:body/>
<hr/>
<script src="<c:url value="/resources/lib/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/lib/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
