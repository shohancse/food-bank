
CREATE DATABASE  IF NOT EXISTS `fly_leaf` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `fly_leaf`;
-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: fly_leaf
-- ------------------------------------------------------
-- Server version	5.5.53-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `id`            BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `activity_type` VARCHAR(255) NOT NULL,
  `created_at`    DATETIME     NOT NULL,
  `message`       VARCHAR(255) NOT NULL,
  `user_id`       BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_csf0njobhrvcaaryhn9ya8vje` (`user_id`),
  CONSTRAINT `FK_csf0njobhrvcaaryhn9ya8vje` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (1,'UPDATE','2016-01-01 12:00:00','test123',1);
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_token`
--

DROP TABLE IF EXISTS `auth_token`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_token` (
  `id`         BIGINT(20) NOT NULL AUTO_INCREMENT,
  `expires_on` DATETIME DEFAULT NULL,
  `token`      VARCHAR(255) DEFAULT NULL,
  `user_id`    BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_aiqc20kpjasth5bxogsragoif` (`user_id`),
  CONSTRAINT `FK_aiqc20kpjasth5bxogsragoif` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_token`
--

LOCK TABLES `auth_token` WRITE;
/*!40000 ALTER TABLE `auth_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `author`
--

DROP TABLE IF EXISTS `author`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author` (
  `id`        BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `details`   VARCHAR(255) DEFAULT NULL,
  `image_url` VARCHAR(255) NOT NULL,
  `name`      VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =5
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author`
--

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` VALUES (1, 'Lorem Ipsum', '', 'Andrew Hunter'), (2, 'Lorem Ipsum', '', 'Aalen Harper'),
  (3, 'Lorem Ipsum', '', 'S Salvihanan'), (4, 'Lorem Ipsum', '', 'Muhammad Zafar Iqbal');
/*!40000 ALTER TABLE `author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `author_book`
--

DROP TABLE IF EXISTS `author_book`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author_book` (
  `author_id` BIGINT(20) NOT NULL,
  `book_id`   BIGINT(20) NOT NULL,
  KEY `FK_bfff0qy5yokmeel0q0e10msv7` (`book_id`),
  KEY `FK_3756au12qe5s0ee22r4tbfj54` (`author_id`),
  CONSTRAINT `FK_3756au12qe5s0ee22r4tbfj54` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`),
  CONSTRAINT `FK_bfff0qy5yokmeel0q0e10msv7` FOREIGN KEY (`book_id`) REFERENCES `author` (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author_book`
--

LOCK TABLES `author_book` WRITE;
/*!40000 ALTER TABLE `author_book` DISABLE KEYS */;
INSERT INTO `author_book` VALUES (1, 1), (1, 3);
/*!40000 ALTER TABLE `author_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `id`           BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `country`      VARCHAR(255) DEFAULT NULL,
  `edition`      VARCHAR(255) DEFAULT NULL,
  `image_url`    VARCHAR(255) NOT NULL,
  `isbn`         VARCHAR(255) DEFAULT NULL,
  `language`     VARCHAR(255) NOT NULL,
  `no_of_pages`  INT(11) DEFAULT NULL,
  `title`        VARCHAR(255) NOT NULL,
  `publisher_id` BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_19ss4s5ji828yqdpgm0otr93s` (`publisher_id`),
  CONSTRAINT `FK_19ss4s5ji828yqdpgm0otr93s` FOREIGN KEY (`publisher_id`) REFERENCES `publisher` (`id`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =11
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1, 'Bangladesh', '1st', '', '235235', 'Bengali', 255, 'Sharp Objects', 1),
  (3, 'Denmark', '3rd', '', '548999', 'Bengali', 300, 'Sonet of Love', 2),
  (4, 'Bangladesh', '4th', '', '3546546', 'Bengali', 300, 'Visual Basic', 3),
  (5, 'India', '5th', '', '246876', 'English', 456, 'Computer FUndamentals Hardware and Programming', 3),
  (6, 'India', '9th', '', '5464354', 'Bengali', 500, 'Partition and Bengal', 1),
  (7, 'Bangladesh', '1st', '', '516546', 'Bengali', 100, '50 Years of Politics: Through My Eyes', 1),
  (8, 'Denmark', '1st', '', '53513', 'English', 50, 'Song of Stars', 1),
  (9, 'Germany', '1st', '', '24564', 'English', 523, 'Relativity- The Special and The General Theory', 2),
  (10, 'Bamgladesh', '1st', '', '45646', 'Bengali', 60, 'Mathematics Olympiad', 1);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UPDATE `fly_leaf`.`book` SET `title`='Mexican Hot Pizza' WHERE `id`='1';
UPDATE `fly_leaf`.`book` SET `title`='Oven Baked Chicken Breast Pasta' WHERE `id`='3';
UPDATE `fly_leaf`.`book` SET `title`='Chicken Shawarma Wrap' WHERE `id`='4';
UPDATE `fly_leaf`.`book` SET `title`='Barcelona Pizza' WHERE `id`='6';
UPDATE `fly_leaf`.`book` SET `title`='Chicken Cream Pasta' WHERE `id`='7';
UPDATE `fly_leaf`.`book` SET `title`='Oven Baked Shawarma Beef Pasta' WHERE `id`='8';
UPDATE `fly_leaf`.`book` SET `title`='Americana Pizza' WHERE `id`='9';
UPDATE `fly_leaf`.`book` SET `title`='Olive Pizza' WHERE `id`='10';
UPDATE `fly_leaf`.`book` SET `title`='Chicken Salad' WHERE `id`='5';


UNLOCK TABLES;

--
-- Table structure for table `book_author`
--

DROP TABLE IF EXISTS `book_author`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_author` (
  `book_id`   BIGINT(20) NOT NULL,
  `author_id` BIGINT(20) NOT NULL,
  KEY `FK_6cmg2roopa9a4c97uxetgf2e9` (`author_id`),
  KEY `FK_q37qkj7serxg0bh56m450uigs` (`book_id`),
  CONSTRAINT `FK_6cmg2roopa9a4c97uxetgf2e9` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`),
  CONSTRAINT `FK_q37qkj7serxg0bh56m450uigs` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_author`
--

LOCK TABLES `book_author` WRITE;
/*!40000 ALTER TABLE `book_author` DISABLE KEYS */;
INSERT INTO `book_author` VALUES (1, 1), (3, 1), (5, 2), (6, 2), (9, 4), (10, 4), (7, 3), (8, 3);
/*!40000 ALTER TABLE `book_author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_product`
--

DROP TABLE IF EXISTS `book_product`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_product` (
  `book_id`        BIGINT(20) NOT NULL,
  `productList_id` BIGINT(20) NOT NULL,
  UNIQUE KEY `UK_33mbyx166gqxe9greh2hk95tn` (`productList_id`),
  KEY `FK_1uikgbta4n8ca519nfw0ym56r` (`book_id`),
  CONSTRAINT `FK_1uikgbta4n8ca519nfw0ym56r` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  CONSTRAINT `FK_33mbyx166gqxe9greh2hk95tn` FOREIGN KEY (`productList_id`) REFERENCES `product` (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_product`
--

LOCK TABLES `book_product` WRITE;
/*!40000 ALTER TABLE `book_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `book_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_sub_category`
--

DROP TABLE IF EXISTS `book_sub_category`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_sub_category` (
  `book_id`         BIGINT(20) NOT NULL,
  `sub_category_id` BIGINT(20) NOT NULL,
  KEY `FK_b5c699r6df2byji3rnvvbx02t` (`sub_category_id`),
  KEY `FK_g3r9p0u601y6j1xvr6lofvig` (`book_id`),
  CONSTRAINT `FK_b5c699r6df2byji3rnvvbx02t` FOREIGN KEY (`sub_category_id`) REFERENCES `subcategory` (`id`),
  CONSTRAINT `FK_g3r9p0u601y6j1xvr6lofvig` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_sub_category`
--

LOCK TABLES `book_sub_category` WRITE;
/*!40000 ALTER TABLE `book_sub_category` DISABLE KEYS */;
INSERT INTO `book_sub_category` VALUES (1, 1), (3, 3), (4, 5), (5, 7), (6, 9), (7, 11), (8, 12), (9, 13), (10, 14);
/*!40000 ALTER TABLE `book_sub_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `id`              BIGINT(20) NOT NULL AUTO_INCREMENT,
  `quantity`        INT(11)    NOT NULL,
  `sub_total_price` DOUBLE DEFAULT NULL,
  `product_id`      BIGINT(20) DEFAULT NULL,
  `user_id`         BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ey2t23ju6wbpsypqcd6rnm0go` (`product_id`),
  KEY `FK_9emlp6m95v5er2bcqkjsw48he` (`user_id`),
  CONSTRAINT `FK_9emlp6m95v5er2bcqkjsw48he` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_ey2t23ju6wbpsypqcd6rnm0go` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id`   BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_46ccwnsi9409t36lurvtyljak` (`name`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =6
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category`
VALUES (2, 'Computer, Internet & Freelancing'), (3, 'History In Tradition'), (4, 'Mathematics, Science & Technology'),
  (1, 'Translation');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id`                   BIGINT(20) NOT NULL AUTO_INCREMENT,
  `bkash_transaction_id` VARCHAR(255) DEFAULT NULL,
  `checkout_time`        DATETIME DEFAULT NULL,
  `delivery_date`        DATETIME DEFAULT NULL,
  `destination`          VARCHAR(255) DEFAULT NULL,
  `order_status`         VARCHAR(255) DEFAULT NULL,
  `order_type`           VARCHAR(255) DEFAULT NULL,
  `payment_option`       VARCHAR(255) DEFAULT NULL,
  `shipping_cost`        DOUBLE DEFAULT NULL,
  `total_price`          DOUBLE DEFAULT NULL,
  `user_id`              BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_mh40cn97o5svvy5c32ws9tnvp` (`user_id`),
  CONSTRAINT `FK_mh40cn97o5svvy5c32ws9tnvp` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordered_product`
--

DROP TABLE IF EXISTS `ordered_product`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordered_product` (
  `id`              BIGINT(20) NOT NULL AUTO_INCREMENT,
  `quantity`        INT(11)    NOT NULL,
  `sub_total_price` DOUBLE DEFAULT NULL,
  `order_id`        BIGINT(20) DEFAULT NULL,
  `product_id`      BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_2403i5fqpo2rg62srhavu18t3` (`product_id`),
  KEY `FK_nr42e4pbkd60bjey5ta1rssey` (`order_id`),
  CONSTRAINT `FK_2403i5fqpo2rg62srhavu18t3` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_nr42e4pbkd60bjey5ta1rssey` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordered_product`
--

LOCK TABLES `ordered_product` WRITE;
/*!40000 ALTER TABLE `ordered_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordered_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id`             BIGINT(20) NOT NULL AUTO_INCREMENT,
  `avg_rating`     DOUBLE DEFAULT NULL,
  `base_price`     DOUBLE DEFAULT NULL,
  `discount`       DOUBLE     NOT NULL,
  `purchase_price` DOUBLE DEFAULT NULL,
  `rating_count`   INT(11) DEFAULT NULL,
  `stock_quantity` INT(11) DEFAULT NULL,
  `book_id`        BIGINT(20) DEFAULT NULL,
  `store_id`       BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_6j1r1vq1htsy8wd7lrwvgl1nj` (`book_id`),
  KEY `FK_j9qchw9ki2is6psdc7uuujyqx` (`store_id`),
  CONSTRAINT `FK_6j1r1vq1htsy8wd7lrwvgl1nj` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  CONSTRAINT `FK_j9qchw9ki2is6psdc7uuujyqx` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =11
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product`
VALUES (1, 4.5, 120, 0, 120, 1, 5, 1, 1), (2, 3.5, 150, 0, 150, 1, 10, 3, 1), (4, 4, 200, 0, 200, 1, 1, 4, 3),
  (5, 2.5, 210, 0, 210, 1, 2, 5, 3), (6, 2.5, 150, 0, 150, 1, 3, 6, 4), (7, 5, 140, 0, 140, 1, 4, 7, 4),
  (8, 2, 300, 0, 300, 1, 1, 8, 5), (9, 1.5, 123, 0, 123, 1, 20, 9, 5), (10, 0, 230, 0, 230, 1, 3, 10, 5);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publisher`
--

DROP TABLE IF EXISTS `publisher`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publisher` (
  `id`             BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `image_url`      VARCHAR(255) DEFAULT NULL,
  `name`           VARCHAR(255) NOT NULL,
  `publisher_info` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_h9trv4xhmh6s68vbw9ba6to70` (`name`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =4
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publisher`
--

LOCK TABLES `publisher` WRITE;
/*!40000 ALTER TABLE `publisher` DISABLE KEYS */;
INSERT INTO `publisher`
VALUES (1, NULL, 'Creative Media Publisher', 'Lorem Ipsum'), (2, '', 'T-Fountain', 'Lorem Ipsum'),
  (3, '', 'O\'Reilly Media Ink', 'Engineering');
/*!40000 ALTER TABLE `publisher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id`   BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8sewwnpamngi6b1dwaa88askk` (`name`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =3
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1, 'Admin'), (2, 'User');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_log`
--

DROP TABLE IF EXISTS `sale_log`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale_log` (
  `id`         BIGINT(20) NOT NULL AUTO_INCREMENT,
  `sold_at`    DATETIME   NOT NULL,
  `quantity`   BIGINT(20) NOT NULL,
  `sale_price` DOUBLE     NOT NULL,
  `product_id` BIGINT(20) DEFAULT NULL,
  `user_id`    BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1u7hwpm0di8masdqfcvxcci1v` (`product_id`),
  KEY `FK_d0arsejaacfvpdipf61cfgk0t` (`user_id`),
  CONSTRAINT `FK_1u7hwpm0di8masdqfcvxcci1v` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_d0arsejaacfvpdipf61cfgk0t` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_log`
--

LOCK TABLES `sale_log` WRITE;
/*!40000 ALTER TABLE `sale_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `sale_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store` (
  `id`        BIGINT(20) NOT NULL AUTO_INCREMENT,
  `image_url` VARCHAR(255) DEFAULT NULL,
  `name`      VARCHAR(255) DEFAULT NULL,
  `status`    VARCHAR(255) DEFAULT NULL,
  `user_id`   BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_d0p5ly1cv6guij7sq1mbnr8ec` (`name`),
  KEY `FK_bjqth9sjo3phwbmybuysm65be` (`user_id`),
  CONSTRAINT `FK_bjqth9sjo3phwbmybuysm65be` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =6
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store`
--

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store`
VALUES (1, '', 'Store1', 'APPROVED', 2), (3, '', 'Store2', 'APPROVED', 2), (4, '', 'Normal User Store1', 'APPROVED', 4),
  (5, '', 'Normal User Store2', 'APPROVED', 4);

UPDATE `fly_leaf`.`store` SET `name`='Pizza Guy' WHERE `id`='1';
UPDATE `fly_leaf`.`store` SET `name`='Tokyo Express' WHERE `id`='3';
UPDATE `fly_leaf`.`store` SET `name`='Star Kabab' WHERE `id`='4';
UPDATE `fly_leaf`.`store` SET `name`='Take Out' WHERE `id`='5';

/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategory`
--

DROP TABLE IF EXISTS `subcategory`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcategory` (
  `id`          BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `name`        VARCHAR(255) NOT NULL,
  `category_id` BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_e060alu3238gwu0mvhgh6xkhd` (`name`),
  KEY `FK_dglte9qeu8l5fhggto4loyegg` (`category_id`),
  CONSTRAINT `FK_dglte9qeu8l5fhggto4loyegg` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =15
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategory`
--

LOCK TABLES `subcategory` WRITE;
/*!40000 ALTER TABLE `subcategory` DISABLE KEYS */;
INSERT INTO `subcategory`
VALUES (1, 'Novel', 1), (3, 'Poem', 1), (5, 'Software', 2), (7, 'Hardware & Troubleshooting', 2), (9, 'Indian', 3),
  (11, 'Liberation War', 3), (12, 'Cosmology', 4), (13, 'Physics', 4), (14, 'Mathematics', 4);
/*!40000 ALTER TABLE `subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `bio` varchar(255) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `is_approved` bit(1) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =5
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` VALUES (1,'Dhaka','I m admin','1993-07-03 00:00:00','admin@therapservices.net','Admin','MALE','','','','admin','01945','2016-12-26 07:04:33'),(2,'Sylhet','I m shopkeeper','1993-09-01 00:00:00','user@therapservices.net','Joe','MALE','','','Smith','user','091374','2016-01-01 06:00:00'),(3,'Dhaka','I m not approved yet','1992-09-07 00:00:00','unapproved@therapservices.net','Lila','FEMALE','','\0','Boti','user','45412','2016-01-01 06:00:00'),(4,'Dhaka','Normal User and Shopkeeper','2015-12-25 10:36:06','normal@therapservices.net','Mir','MALE','','','Kashim','user','23425','2016-01-01 06:00:00');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_product_rating`
--

DROP TABLE IF EXISTS `user_product_rating`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_product_rating` (
  `id`           BIGINT(20) NOT NULL AUTO_INCREMENT,
  `rating_value` FLOAT DEFAULT NULL,
  `product_id`   BIGINT(20) DEFAULT NULL,
  `user_id`      BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_5e2dt2ncmomhxxe8i1xf2xqw4` (`product_id`),
  KEY `FK_ikl6ycp805md4ht57yp32opog` (`user_id`),
  CONSTRAINT `FK_5e2dt2ncmomhxxe8i1xf2xqw4` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_ikl6ycp805md4ht57yp32opog` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =10
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_product_rating`
--

LOCK TABLES `user_product_rating` WRITE;
/*!40000 ALTER TABLE `user_product_rating` DISABLE KEYS */;
INSERT INTO `user_product_rating`
VALUES (1, 5, 1, 4), (2, 3.5, 2, 4), (3, 4.5, 4, 4), (4, 2.5, 5, 4), (5, 4, 6, 4), (6, 1.5, 7, 4), (7, 5, 8, 4),
  (8, 1.5, 9, 4), (9, 0, 10, 4);
/*!40000 ALTER TABLE `user_product_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `user_id` BIGINT(20) NOT NULL,
  `role_id` BIGINT(20) NOT NULL,
  KEY `FK_it77eq964jhfqtu54081ebtio` (`role_id`),
  KEY `FK_apcc8lxk2xnug8377fatvbn04` (`user_id`),
  CONSTRAINT `FK_apcc8lxk2xnug8377fatvbn04` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_it77eq964jhfqtu54081ebtio` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verification_token`
--

DROP TABLE IF EXISTS `verification_token`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verification_token` (
  `id`         BIGINT(20) NOT NULL AUTO_INCREMENT,
  `expires_on` DATETIME DEFAULT NULL,
  `is_used`    BIT(1) DEFAULT NULL,
  `token`      VARCHAR(255) DEFAULT NULL,
  `user_id`    BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_q6jibbenp7o9v6tq178xg88hg` (`user_id`),
  CONSTRAINT `FK_q6jibbenp7o9v6tq178xg88hg` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verification_token`
--

LOCK TABLES `verification_token` WRITE;
/*!40000 ALTER TABLE `verification_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `verification_token` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2016-12-26 13:06:55
